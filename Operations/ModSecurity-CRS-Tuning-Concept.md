---
layout: article
title: ModSecurity CRS-Tuning-Concept
author: © 2022 Swiss Post Ltd.
date: "2022-06-24"

toc-own-page: true
titlepage: true
titlepage-rule-color: "ffcc00"
titlepage-text-color: "ffffff"
footer-left: "Swiss Post"
titlepage-background: ".gitlab/media/swisspostbackground.png"
...

# ModSecurity CRS-Tuning-Concept

## Introduction

This document describes the procedure for tuning the reverse proxies.

## Starting position and goal

The _Apache httpd_ reverse proxies use the _ModSecurity_ module in combination with the _OWASP ModSecurity Core Rule Set (CRS)_ to filter both incoming requests and their responses.
In addition to the CRS, other ModSecurity rules and Apache httpd configurations are in use, but they are not in the scope of this document.

One of the difficulties in the operation of CRS is the false positives, i.e. when rules are triggered by legitimate requests.
This document describes the procedure for eliminating false positives and the considerations that go into this procedure.

## Explanation of the most important technical parameters

The functionality of the CRS rules can be influenced by certain parameters.
The most important of these parameters are described here.

### What are paranoia levels?

The CRS consists of approximately 200 ModSecurity rules, which are divided into four groups: Paranoia Level 1 (PL1) to Paranoia Level 4 (PL4).
With a higher paranoia level, the sensitivity of the rules increases, which in turn leads to a higher probability of false positives.
This means that the rules at PL1 cause few false positives, but can be circumvented by sophisticated attackers.
PL4 rules cause significantly more false positives, but it is also significantly more difficult for attackers to circumvent the rules.

### What are anomaly score limits?

CRS rule violations do not immediately lead to requests being blocked.
Instead, the anomaly score of the request is increased with each rule violation.
The anomaly score thus indicates how conspicuous a request is concerning potential attacks.
Only if a request exceeds the defined limit is the request blocked.
The scores of the rule violations are added up separately for requests, and their responses and the limits can be defined separately.

## The tuning

The term tuning denotes the suppression of false positives.
This means that false positives are to be prevented to prevent legitimate requests from being blocked.

### Tuning principles

The goal is to ensure that all attacks are reliably blocked without ever mistakenly blocking legitimate requests from end-users.

In practice, this goal is difficult to achieve, but the following approach can yield a configuration that comes very close.

### Procedure

ModSecurity has a mode in which rule violations are only written to the log but never blocked.
This mode is not used here; ModSecurity is used in the so-called blocking mode right from the start.
In a first step, the anomaly score limit in the internally accessible integration environment is set high enough to prevent blocking by rule violations.
Subsequently, the limit is reduced to the target value over several tuning iterations.

Before each reduction, the reported score values in the logs are used to check whether legitimate requests would reach the new limit.
The reduction may be carried out only if it can be ruled out that legitimate requests will be blocked in this way.

This approach allows false positives to be registered in the background but does not prevent the testers from working, as no requests are blocked.

In the case of e-voting, tuning is facilitated by the fact that the application uses only a small number of free-text entries and the application is tested extensively.
False positives are often limited to fields containing cryptographic tokens or similar.

False positives are eliminated by adding so-called rule exclusions.
It is important to proceed carefully, as a rule, an exclusion that is too broad can accidentally short-circuit the WAF.
The rule exclusions are therefore formulated as narrowly as possible, such that the set of rules remains functional.
It is hardly possible to define a general policy on how the rule exclusions should be formulated, but it is nevertheless possible to establish certain guidelines.
Such guidelines are discussed below.

Only when the limit has been tuned down to the target value may the configuration be used in production.
The rule violations must be also analysed later in the production environment to suppress potential false positives that were not triggered during testing.

## Version, paranoia level and anomaly score limits

For e-voting, the latest CRS version is used as soon as it has been internally validated and released.
The paranoia level is set to 4, and the anomaly score limit for both requests and responses is set to 5.

## Documentation of rule exclusions

The documentation of rule exclusions is of particular importance.
The problem is that the alerts disappear when rule exclusions are added.
This means that the WAF becomes blind to these false positives.

For example, if a rule exclusion, i.e. the selective deactivation of a rule to prevent false positives, is to be re-evaluated after two years, it can be reevaluated only by deactivating said rule exclusion and waiting for a false positive to be triggered again.
Of course, this is not desirable in a system with a low anomaly score limit, as it could immediately lead to legitimate requests being blocked.

Therefore, a rule exclusion should be supplemented with an appropriate comment describing the false alarm:

Line 1: Which rule is switched off

Line 2: Extract from the log: Due to which alarm is the rule switched off, or explanation as to why the rule exclusion is needed

Line 3: Rule exclusion directive

```shell
# ModSec Rule Exclusion: 932160 : Remote Command Execution: Unix Shell Code Found
# [data "Matched Data: /bin/bash found within ARGS:pass: /bin/bash"]
SecRule REQUEST_URI "@beginsWith /drupal/index.php/user/login" \
   "phase:2,nolog,pass,id:10000,ctl:ruleRemoveTargetById=932160;ARGS:pass"
```

If there are four or more different values, the first three values are mapped on separate lines and then added to line 4 as follows:

```shell
# Additional data encountered
```

The creation of rule exclusions and explanatory comments can be supported by tooling, but it is not an automated process.

## Create rule exclusions

When creating rule exclusions, it makes sense to work very cautiously and to formulate rule exclusions as narrowly as possible.
However, if a rule is mistakenly violated by different requests, then it makes sense to create broader rule exclusions.
Rule exclusions can generally be divided into three groups: fine-grained, medium-grained, and coarse-grained.

Various ModSecurity directives can be used to create rule exclusions.
Some of them are processed at start-up and others are processed during request handling.
Depending on the time, the rule exclusions must be placed in different locations concerning the rules of the CRS.

### Fine-grained

A fine-grained rule exclusion disables a single rule for a specific parameter on a specific path.

Example:

```shell
# ModSec Rule Exclusion: 932160 : Remote Command Execution: Unix Shell Code Found
# [data "Matched Data: /bin/bash found within ARGS:pass: /bin/bash"]
SecRule REQUEST_URI "@beginsWith /drupal/index.php/user/login" \
   "phase:2,nolog,pass,id:10000,ctl:ruleRemoveTargetById=932160;ARGS:pass"
```

This type of rule exclusion must be added in the Apache configuration _before_ the CRS Include statement otherwise, the rule will be executed before the rule exclusion.

### Medium-grained

There are two different variants of medium-grained rule exclusions.
In the first variant, a single rule is turned off for a given parameter, regardless of the path.
In the second variant, a single rule is switched off for a specific path, regardless of the parameter.

Example of the first variant:

```shell
# ModSec Rule Exclusion: 942190 : Detects MSSQL code execution and information gathering attempts
# [data "Matched Data: union select found within ARGS:keys: union select from users"]
SecRuleUpdateTargetById 942190 "!ARGS:keys"
```

This type of rule exclusion must be added in the Apache configuration _after_ the CRS include statement otherwise, the CRS rule is not even loaded when the rule exclusion is to be executed.

Example of the second variant:

```shell
# ModSec Rule Exclusion: 932160 : Remote Command Execution: Unix Shell Code Found
# [data "Matched Data: /bin/bash found within ARGS:pass: /bin/bash"]
SecRule REQUEST_URI "@beginsWith /drupal/index.php/user/login" \
   "phase:2,nolog,pass,id:10000,ctl:ruleRemoveById=932160"
```

This type of rule exclusion must be added in the Apache configuration _before_ the CRS Include statement otherwise, the rule will be executed before the rule exclusion.

### Coarse-grained

A coarse-grained rule exclusion completely disables a single rule or multiple rules.

Alternatively, it applies a fine-grained or medium-grained exclusion rule, as described above, to a multitude of rules.

Example:

```shell
# ModSec Rule Exclusion: 932160 : Remote Command Execution: Unix Shell Code Found
# [data "Matched Data: /bin/bash found within ARGS:pass: /bin/bash"]
SecRuleRemoveById 932160
```

This type of rule exclusion must be added in the Apache configuration after the CRS Include statement otherwise, the CRS rule is not even loaded when the rule exclusion is to be executed.

### Delicate rules

CRS contains rules which appear in the logs very frequently but should not be deactivated completely:

* 941100 XSS Detection via Libinjection
  * very important rule
  * may only be tuned with finely formulated exclusion rules
* 942100 SQLi detection using libinjection
  * very important rule
  * may only be tuned with finely formulated exclusion rules
* 949110 Checking the anomaly score limit of the request
  * essential rule
  * must never be switched off
* 959100 Checking the anomaly score limit of the response
  * essential rule
  * must never be switched off

The individual SQL rules in the 942 block from 942110 to 942999 can be switched off if necessary, as there is certainly redundancy here.
This means that if a single SQL rule is switched off, there is a high chance that another rule will also recognize the same attack; ideally without a false positive.

The same applies to the XSS rules from 941110 to the end of the block at 941999.

Rules 949110 and 959100 are the rules that check the anomaly score limit.
If these rules are switched off, the WAF is short-circuited and will no longer block requests, or their responses, because of CRS rule violations.

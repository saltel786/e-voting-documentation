# About the Disclosure of the Swiss Post's e-voting system

![Logo](.gitlab/media/swisspostlogo.png)

At Swiss Post we believe that when it comes to e-voting transparency is key, to safeguard the greater good of well established democratic process.
Therefore we are openly disclosing not only the source code of our e-voting solutions but also the specification and all documentation we create around this solution.

This is why we initiated a community programme to provide access to the system for independent experts in order to continuously improve the e-voting system. Disclosure facilitates an in-depth examination and dialogue amongst specialists and our experts.
It will enable us to gradually improve the system, by considering findings.
Disclosure at the end of the day strengthens the security of the e-voting system and the digitalization of democracy.  

Please find additional information around the disclosure, announcements and blog posts dedicated [Community website.](https://evoting-community.post.ch/en)

## Overview e-voting disclosure on GitLab

**All the source code, specification and documents are published here on Gitlab, accessible for everyone.**  
  
![Overview](.gitlab/media/Overview.png)


**Proprietary**: The source code is subject to a proprietary licence of Swiss Post Ltd.  

**Open Source**: The source code is subject to an Apache 2.0 licence.

As depicted by the image we are disclosing a multitude of core elements for the overall e-voting solutions, therefore
we have created a series of GitLab groups and projects. For you to quickly retrieve the relevant content leverage this table.

| Project | Description | Status |
| :------- | :---- | :---- |
| [E-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting) | The system that allows electoral authorities to set up an e-voting procedure and voters to cast their vote electronically via computer, smartphone or tablet.| Disclosed |
| [E-voting documentation](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation) | Contains all the core documentation like the architecture of the overall solution, the specification of the e-voting system as well as the cryptographic protocol. | Disclosed |
| [Verifier](https://gitlab.com/swisspost-evoting/verifier/verifier) | Source code and documentation of the independent verification software, which checks whether elections held by means of e-voting have been conducted free of fraud. | Disclosed |
| [Crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) | Source code and documentation of the cryptographic primitives library containing key cryptographic algorithms. Used in both the e-voting system and the separate verification software.| Disclosed |
| [Crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) | Source code in typescript of the cryptographic primitives library containing key cryptographic algorithms. Used in the e-voting system. | Disclosed |
| [Crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain) | The Crypto-primitives-domain library encapsulates the data objects that are produced by the e-voting system and consumed by a verifier. It acts as a documentation of the interface between these two systems. | Disclosed |
  
**Note:** The e-voting system is being disclosed in stages. We will subsequently publish further elements of the system as we move along.  

## Contributing to the projects

We welcome all contributions to our projects.  
Be it findings, improvements or questions you may have.  
Before you contribute, we only ask you to read and comply with our [Code of Conduct](CONTRIBUTING.md).  
All contributions, also the ones collected under the Bug Bounty programme, will be published on GitLab.  
  
### Contribute via GitLab

You can contribute on the GitLab projects directly, find out how [here.](REPORTING.md)

### Contribute via Online form

Alternatively, you can enter your report via an [online form.](https://evoting-community.post.ch/en/contributions/submitting-findings)

### Contribute via encrypted E-Mail

Report your findings to us via encrypted e-mail. [Use the Incamail service for this purpose.](https://evoting-community.post.ch/en/contributions/submitting-findings)

### Contribute under the Bug Bounty programme

In addition to GitLab, we have initiated a continuous reward programme [bug bounty Swiss Post – E-voting](https://yeswehack.com/programs/swiss-post-evoting), with rewards up to **230'000 EUR**.  

Our aim is to reward **security-related findings** you may find while performing:  
  
- a static test of documentation and source code
- dynamically testing a self-deployed and running instance of our system
- while participating in our periodical public intrusion test of the infrastructure
  
Check out the [programme policy](https://yeswehack.com/programs/swiss-post-evoting) for further details and the full reward grid.  

_To submit findings under the Bug Bounty programme identification and registration on our partner platform Yes We Hack is required._  
  
## FAQ  
  
Answers to frequently asked questions on the community programme and definition of terms can be found in the [FAQ](https://evoting-community.post.ch/en/help-and-contact/faq) section of the community website.
  
## Changelog  
  
Please consult our [changelog](CHANGELOG.md) for a summary of all notable changes made to the project.  
  
## Support & contact  
  
If you have any questions or require help, please do not hesitate to [contact](https://evoting-community.post.ch/en/help-and-contact/support#contact) us.
Our specialists will be pleased to answer your questions regarding cryptography, security, documentation, the Code of Conduct or any other subject relating to Swiss Post’s e-voting system.

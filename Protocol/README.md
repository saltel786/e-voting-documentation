# Accompanying documentation for the *Protocol of the Swiss Post Voting System - Computational Proof of Complete Verifiability and Privacy*

## What is the content of this document?

This document describes the cryptographic building blocks and shows how they work together to ensure verifiability and vote privacy. Moreover, it provides a mathematical proof that the cryptographic protocol achieves the desired security objectives under a minimal set of assumptions. The cryptographic proof is a central element of guaranteeing the security of a protocol in modern cryptography.

## What is a protocol?

A [security protocol](https://en.wikipedia.org/wiki/Cryptographic_protocol) (cryptographic protocol or encryption protocol) is an abstract or concrete protocol that performs a security-related function and applies cryptographic methods, often as sequences of cryptographic primitives.

## Why is a cryptographic proof important?

In line with the current best practice in cryptography, the Federal Chancellery requires a cryptographic proof of verifiability and privacy. To cite [Katz and Lindell](https://www.cs.umd.edu/~jkatz/imc.html): *Without proof that no adversary of the specified power can break the scheme, we are left only with our intuition that this is the case. Experience has shown that intuition in cryptography and computer security is disastrous. There are countless examples of unproven schemes that were broken, sometimes immediately and sometimes years after being presented or deployed.*

## Changes in version 1.0.0

Version 1.0.0 fixes the following known issues and incorporates feedback from the Federal Chancellery's mandated experts. The experts' reports can be found under [Reports](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Reports/Examination2021). We want to thank the experts for their high-quality, constructive remarks:

- Vanessa Teague (Thinking Cybersecurity), Olivier Pereira (Université catholique Louvain), Thomas Edmund Haines (Australian National University)
- Aleksander Essex (Western University Canada)
- Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis (Bern University of Applied Sciences)

Version 1.0.0 of the computational proof contains the following changes:

- Updated the security analysis to the latest version of the Federal Chancellery's Ordinance.
- Incorporated the protocol changes from the system specification version 1.0.0
- Added a section to address potential inconsistencies between control components (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines, addresses [GitLab issue #1](https://gitlab.com/swisspost-evoting/verifier/verifier/-/issues/1))
- Improved the clarity of the Return Codes Mapping Table Correctness proof  (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Clarified the definition of Individual Verifiability (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
- Improved the clarity of each individual game hop and included numerous explicit reductions to make the security analysis more convincing (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Changed the formal vote privacy definition from BPRIV to Benaloh (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Removed the redundant protocol description (fixes [GitLab issue #25](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/25))
- Added general improvements and clarifications to the computational proof (addresses [GitLab issue #3](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/3))

## Changes in version 0.9.12

Version 0.9.12 incorporates some of the feedback from the Federal Chancellery's mandated experts (see above)

Version 0.9.12 of the computational proof contains the following changes:

- Updated the threat model and security objectives to the latest version of the Federal Chancellery's Ordinance.
- Detailed the key generation algorithm in the multi-recipient ElGamal setting (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
- Provided additional details and justification to the hash function and key derivation section (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Replaced PBKDF with Argon2 (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
- Clarified the security definitions of the non-interactive zero-knowledge proof systems (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Provided additional details on the Common Reference String and the selection of the parameters of the verifiable mix net (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
- Fixed some errors in the definitions of the computational hardness assumptions (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Fixed minor mistakes and typos in the document.

## Changes in version 0.9.11

Version 0.9.11 incorporates some of the feedback from the Federal Chancellery's mandated experts (see above)

Version 0.9.11 of the computational proof contains the following changes:

- Updated the computational proof to the lastest draft of the Federal Chancellery's Ordinance on Electronic Voting (OEV) (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis). This includes renaming the algorithms SetupTallyPO and GenEncryptionKeysPO to SetupTallyEB and GenSetupEncryptionKeys.
- Clarified the verifiable generation of small primes encoding the voting options (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Rewritten the section on key derivation and hash functions (incorporates feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Simplified the proof of Return Codes Mapping Table Correctness by removing the KDF-advantage in the security analysis (obsolete in the random oracle model).
- Added a hash of the Ballot Casting Key in the algorithms GenVerDat and CreateConfirmMessage to allow the reduction to the SGSP problem in the privacy security analysis (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Provided a section on the mix net's security properties and on how to simulate mix net proofs for perfect special honest verifier zero-knowledge (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Defined a fully collision-resistant RecursiveHash function with a uniform output in G_q (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Removed the DeriveKey method and replaced it with a KDF function based on the HKDF-expand algorithm.
- Updated the SetupVoting algorithm's sequence diagram by separating setup and printing component.
- Clarified the transmission of the election public key to the control components (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Fixed the definition of the decisional Diffie-Hellmann (DDH) game (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Fixed the compliant ballot definition (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
- Streamlined the definition of the auxiliary strings in the zero-knowledge proof systems.
- Fixed minor mistakes and typos in the document.

## Changes in version 0.9.10

Version 0.9.10 of the computational proof contains the following changes:

- Strengthened  the protocol to verify sent-as-intended properties and vote correctness online in the control components. See Gitlab issue [#7](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/7) for further explanations.
- Specified a rollback scenario for incorrectly inserted or discarded votes (tally phase). See Gitlab issue [#8](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/8) for further explanations.
- Expanded the section on two-round return code schemes with alternative scenarios.
- Switched from single-recipient to multi-recipient ElGamal encryption of the Choice Return Codes in the configuration phase.
- Streamlined the usage and name of lists between the computational proofs and the system specifications.
- Improved clarity of sequence diagrams.

## Changes in version 0.9.9

Version 0.9.9 of the computational proof contains the following changes:

- Fixed the error in the GenCMTable algorithm allowing the adversary to learn the set of possible return codes (reported by Thomas Haines in Gitlab [#2](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/2)).
- Added an introduction about two-round return code schemes.
- Referred to standard notions of security (IND-CPA) in the description of the ElGamal and symmetric encryption scheme.
- Provided more explanation on key compression in the ElGamal multi-recipient encryption algorithm. Thomas Haines raised this point in Gitlab issue [#3](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/3).
- Verifiable mix net: Detailed the case of m=1 where the number of mixed votes is prime. Thomas Haines raised this point in Gitlab issue [#3](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/3).
- Added a sequence diagram for the verifications in the voting phase.
- Removed the Schnorr proof of knowledge, since the combination of plaintext equality proof and exponentiation proof prove equivalent knowledge.
- Added additional auxiliary values to the decryption and plaintext equality proof systems. Thomas Haines raised this point in Gitlab issue [#3](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/3).
- Aligned the order of certain algorithms in the protocol description (section 12).
- Removed verification algorithms of untrustworthy components for the sake of readability.
- Expanded the explanations on the length of the voter's codes.
- Fixed smaller typos and erros in various sections.

## Changes since publication in 2019

Since its initial publication in 2019, we have improved our cryptographic protocol to ensure that it is more:

- complete: we have significantly expanded the description of the underlying building blocks (mix net, zero-knowledge proofs, etc.).

- understandable: we have simplified the description of the protocol and aligned key, variable and algorithm names.

- rigorous: we have improved the coherence of the theorems and proofs; for instance, by taking into account multiple voting options.

Furthermore, we have merged in one single document the proofs of complete verifiability and vote secrecy.

## Limitations

Our cryptographic proof demonstrates that the Swiss Post Voting System guarantees complete verifiability and vote privacy.

However, no cryptographic protocol is unconditionally secure. The protocol of the Swiss Post Voting System bases its security objectives on a few assumptions. Even though these assumptions comply with the [Federal Chancellery's Ordinance on Electronic Voting](https://www.admin.ch/opc/en/classified-compilation/20132343/index.html), we will highlight some of them here for the sake of clarity and transparency:

- First, we are defending our security properties against an adversary that is polynomially bounded. An efficient, fault-tolerant quantum computer could break some of the mathematical assumptions underpinning the Swiss Post Voting System. Quantum-resistant e-voting protocols exist, but they are relatively recent, and their security properties are less understood. We consider quantum-resistance an area for future improvement of the protocol. [The Federal Chancellery's *Summary of the Expert Dialog 2020*, section 11.2.11 addresses this topic](https://www.newsd.admin.ch/newsd/message/attachments/63915.pdf).

- Second, for the purpose of providing vote secrecy, the protocol of the Swiss Post Voting System assumes that the attacker is not controlling the voting client. While we guarantee individual verifiability even if the voting client is malicious, we cannot use cryptography to prevent attackers from spying on the voter's choices on malicious clients. However, the Swiss Post Voting System prevents the server from breaking vote secrecy if at least one control component is honest.

- Third, the protocol of the Swiss Post Voting System assumes a trustworthy setup component. The setup component generates keys and codes in conjunction with the control components. By its nature, the setup component handles sensitive data, and the process for generating and sending voting cards must meet [rigorous requirements](https://www.bk.admin.ch/dam/bk-intra/de/dokumente/pore/politische_rechte/anforderungskatalogdruckereien.pdf.download.pdf/anforderungskatalogdruckereien.pdf). In general, e-voting systems aim to distribute trust and detect any attack or malfunction if at least one control component works correctly. However, we believe that there are certain limits for distributing the functionality of the setup and printing component: one cannot expect the voter to combine different code sheets by hand, and the costs of printing multiple code sheets in independent printing facilities would be currently prohibitive. For now, we propose to implement a return code scheme with a single setup and printing, which must put organizational and technical means into place to prevent leaking the secret codes. * The reduction of trust assumptions in the setup component is part of the [final report of the e-voting steering committee of cantons and confederation for the revision of the current conditions, Measure A.5](https://www.newsd.admin.ch/newsd/message/attachments/64680.pdf).

## Subgroup Generated by Small Primes (SGSP)

The Swiss Post Voting System's computational privacy proof relies on the Subgroup Generated by Small Primes (SGSP) problem. The SGSP problem is related to the Decisional Diffie-Hellmann (DDH) problem. However, the SGSP problem has been studied less in the academic literature than DDH.
Pierrick Gaudry from CNRS/LORIA, a renowned expert in discrete logarithm problems, wrote a [short article](./sgsp.pdf) discussing the SGSP problem.

---
layout: article
title: Software development process for the Swiss Post voting system
author: © 2022 Swiss Post Ltd.
date: "2022-07-29"

toc-own-page: true
titlepage: true
titlepage-rule-color: "ffcc00"
titlepage-text-color: "ffffff"
footer-left: "Swiss Post"
titlepage-background: ".gitlab/media/swisspostbackground.png"
...

# Software development process for the Swiss Post Voting System

## Introduction

This document describes the software development process for the Swiss Post voting system. Furthermore, it provides information on the agile software development approach, explains the tools used and shows the quality aspects of the development process.

The Swiss Post Voting System enables eligible voters to participate electronically in votes and elections. The vote is cast via the web browser on a computer, smartphone or tablet. The various software components and the system architecture are explained in the [system specification and system architecture documents](../System).

## Post Agile Methodology (PAM)

Software Development applies the Post Agile Methodology (PAM). The e-voting team uses the agile PAM approach during development. The method is mainly based on Scrum and focuses on developing products and solutions in an empirical, incremental and iterative process up to market maturity.

The agile practices help users to realise the promises of agile methods, such as "more flexibility", "more ownership", "more value for the customer" and "higher motivation in the team" while meeting the needs of a larger organisation with respect to governance and control.

There is no uniform understanding of what agile practices and methods mean. The Post Agile Methodology Swiss Post uses the following definitions.

**Agile values** -- form the foundation and describe the value system behind the agile way of working

**Agile principles** -- are based on the agile values and describe principles for action

**Agile methods** -- give agile practices an overall structure right up to project management

**Agile practices** (techniques) -- are concrete implementations of agile principles

![Graphic Overview Methods and Practices](../.gitlab/media/software_development_process/image2.png)

In the Post Agile Methodology, principles and practices are thus combined into a coherent process. Five roles, the sponsor, the product owner, the Scrum Master, the development team and the project controller, are on the graphic. As can be seen in the figure below, the product owner, the development team and the Scrum master form the Scrum team. The sponsor and financial controller work closely with the Scrum team but are not included. The same applies to supporting roles that are only called upon selectively.

![Graphic overview of roles in the Post Agile Methodology (PAM)](../.gitlab/media/software_development_process/image3.png)

The Product Owner is responsible for the product and prioritising the backlog. The Scrum Master is also part of the team and has the task of supporting the team in addressing and removing obstacles and helping to apply the agile principles and methods. A sponsor is defined to manage the financial matters and the requirements for the product.

### Scrum

The ceremonies are carried out using the Scrum method. The requirements, specifications and bug fixes are recorded as user stories or tasks in Jira. These stories form the product backlog. The prioritisation of the backlog is done by the product owner.

Development is carried out in defined Sprints. A Sprint is a defined period of time -- for example, two weeks. The Scrum team defines the user stories to be completed in a Sprint. During a sprint, the Scrum team meets daily to discuss questions or obstacles. After a Sprint, a Sprint review is conducted to check the work on the product. A Sprint retrospective is also conducted to address and learn from positive and negative points. The following graphic gives an overview of Scrum.

![Graphic overview of Scrum](../.gitlab/media/software_development_process/image4.png)

## Transparent Software Development

The chapter provides overviews on Transparent Software Development for the Swiss Post voting system. The source code of the voting system is published as part of a community programme. The community programme aims to make access easy for independent experts, continually improve the system and enable dialogue between experts and the Swiss Post e-voting team. It also provides transparency and insight into the functions of the voting system. The community and interested parties can access and inspect the source code. Furthermore, changes from the community or interested parties can be integrated into the source code.

A dedicated source code repository, "GitLab", is in place for the source code publication. On this repository are multiple branches used. The branches are the *develop* branch and the *master* branch. The *develop* branch contains the current development status of the software. The *master* branch has the recent versions/releases. The following graphic illustrates the publication source code repository. 

![Graphic overview GitLab branches](../.gitlab/media/software_development_process/image6.png)

The Transparent Software Development approach supports the following points.

**Permanent publication**

- Swiss Post permanently discloses software releases
- The releases are disclosed on the *master* branch on GitLab and are publicly available
- A history between releases is available to track the changes
- The granularity is “one commit per release"

**Software Increment**

- Software increments are published between releases and are made available on a designated branch (*develop*). 
- An increment is a defined feature/task designed to make it easier to understand the changes in the source code.
- The Product Owner (PO) defines the granularity of that task.
- The name of the commit is the name of the task.
- Publication of the *develop* branch usually occurs every two weeks

**Commit History**

- The commits history for the releases and software increments are published. 
- For all files, the commit history can be examined either per release or per software increment.
- Per version/release, the *commit* history is available on the GitLab *master* branch
  - As an example: commit overview on e-voting (master branch): <https://gitlab.com/swisspost-evoting/e-voting/e-voting/-/commits/master>
- Per software increments, is the commit history available on the GitLab *develop* branch
  - As an example: commit overview on e-voting (develop branch): <https://Gitlab.com/swisspost-evoting/e-voting/e-voting/-/commits/develop>

**Contribution Community**

The contributions from the community (e.g. pull requests) are reviewed, and if accepted, integrated into the e-voting source code. This means that changes or improvements from the community can be included in the source code.

The community has to possibility to contribute new functions or improvements to the voting system.
A contribution can be done with a "Pull Request".
The "Pull Request" will be analysed from the Swiss Post's software development. If approved the contribution from the community are published on the *develop* branch on GitLab with a name identifying the pull-request. 
Further information can be found here: <https://evoting-community.post.ch/>

## Secure Software Development

Security is an integral part of development. Swiss Post relies on the Open Web Application Security Project®, OWASP, a non-profit organisation with a large community, as its security standard and framework. One of the OWASP projects or frameworks is the Software Assurance Maturity Model, SAMM. It considers the different areas from requirements, design and architecture to testing, build and deployment. Best practices and guidelines exist for all areas and are integrated into the development process. With SAMM, the maturity level can be determined. The assessments are carried out regularly, enabling benchmarking and continuous improvements. 

Further information on OWSP and SAMM can be found here "https://owasp.org/www-project-samm/"

## Trusted Software Build

The Trusted Software Build is an organisational approach that describes procedural and organisational measures to prove that the code running on the production system corresponds to the published source code. To this end, a traceable method is applied for making the system available from the source code through to its installation in production (build and deployment). The Trusted Bild implementation for the voting system is based on a "Reproducible Build" approach.

The reproducible build is understood to be a process for deterministic compilation of the source code. The reproducible build is important for cooperation with the community. The public can use it to check and be sure that the published version is the same as the one created. For this purpose, hash values generated during the compilation are compared. To make a reproducible build possible, it is ensured during development that no non-determinism elements are used (e.g. time stamps in a manifest file). More information on reproducible builds can be found here: <https://reproducible-builds.org/>

For more information on the Trusted Build, see the document “[Trusted Build of the Swiss Post Voting System”](../Operations).

## Quality aspects in software development

This chapter deals with the quality aspects of software development. Some quality specifications are prescribed by the PAM (Scrum), others are implemented through the use of software tools and, in addition, there are controls that are carried out by the developer, such as the four-eyes principle.

Swiss Post carries out quality management and has a quality management system. The quality aspects mentioned are described in more detail below. Furthermore, the source code ist externally audited by a third-party company to ensure traceability and auditability.

### Quality management

The process quality is defined by the organisational and product requirements. All processes are documented and reviewed on a regular basis. The process documentation follows company-wide standards.

#### Staff training

One quality aspect is training. The developers and technical staff are trained with regard to internal requirements. This includes PAM training and training on, among other things, the Code of Conduct, safety and development standards.

#### PAM - Scrum

Scrum ensures the quality of changes with a Definition of Ready and a Definition of Done. Every user story, bug or task that represents a change to be implemented, a bug fix or a new function is checked against these definitions.

##### Definition of Ready and Definition of Done

The Definition of Ready (DoR) is defined by the team in advance and applies to all user stories. The Definition of Ready contains criteria that must be fulfilled in order for a user story to be implemented. These criteria include:

- Is the story clearly understandable for everyone involved?

- Have the acceptance criteria been defined?

- Are the acceptance criteria understandable for everyone involved?

- Have possible external dependencies been eliminated?

As soon as a user story passes these tests, it can be implemented.

The same procedure applies to a user story where the implementation is complete. The implementation is checked against the Definition of Done (DoD). The definitions are as follows.

##### Definitions

- **Definition of Ready (DoR)**
  - Mock-ups are defined for new screens or big changes
  - Non-functional requirements are defined. For example, metrics should be defined for verification (e.g. response time)
  - Functional requirements are defined. Complete functional specifications include alternative forms of the expected product formulated in the Jira-issue or referenced in the specification
  - Functional constraints are defined. Pre- and post-constraints are identified
  - Consequences are defined. Any consequences for other projects or components are defined and communicated to the same (e.g. third-party control component)
  - Dependencies are defined. Any dependencies from other project parties are known and fit into our project timeframe (e.g. Verifier)
  - Task specificity. Specificities are identified (e.g. cryptography)
  - Acceptance criteria are defined. Definition of what must be fulfilled to have the story completed and verified appropriately (i.e. Test Management involvement is required).
  - The task is estimated. The story (including subtasks) is estimated during the sprint planning at the latest
- **Definition of Done (DoD)**
  - Algorithms aligned. Our code follows the algorithms\' specification precisely. Comments in the code justify deviations -- which have to be minor and accepted by the product owner
  - Terminology aligned. Class, variable, method, and argument names align closely with the specification and the naming conventions
  - Coding style aligned. We strive for a clear, concise, and consistent coding style across the entire codebase. To this end, we document best practices and peer-review our code extensively
  - Architecture aligned. We carefully evaluate the risks and benefits of using third-party libraries and frameworks. In cryptographically sensitive code, we limit the usage of external dependencies to a strict minimum
  - Auditable. We acknowledge that a wide range of experts is going to scrutinise our code. Therefore, we make our code as understandable as possible: we structure our code systematically, keep our methods small, define clear interfaces, disentangle our components, and eliminate duplication and domain-specific jargon
  - Robust. Our algorithms shall be robust by design; we validate inputs extensively and pay special attention to edge cases
  - Tested. We test our code systematically and broadly. When writing tests, we keep in mind that parts of the system might be malicious. We verify security-sensitive operations and cryptographic code even more extensively
  - Tool-checked. Our code adheres to coding best practices and successfully passes code quality tools (Sonar, Fortify, X-Ray)
  - Peer-reviewed. At least one architect and one developer approve each code modification. Furthermore, a cryptography expert must review cryptographically-sensitive code
  - Sensibly commented. Our comments in the code are clear, correct, and facilitate the understanding of the underlying code. We eliminate superfluous comments or those that contain person-identifiable information.

###### Retrospective

Another quality aspect is the retrospective ceremonies. The retrospective enables the Scrum team to improve continuously from sprint to sprint. The definition according to the Scrum manual is as follows: "The purpose of the sprint retrospective is to plan ways to improve quality and effectiveness. The Scrum team examines how the last sprint went in terms of people, interactions, processes, tools and their definition of done."

#### The four-eyes principle and the segregation of duties

For software development, quality control is implemented according to the dual control principle. Every code change is checked by other developers and/or architects. The checks take place during the commits to the development environment. This ensures that every change is checked and released before synchronisation.

Another quality and security control is the segregation of duties between the development team, test management and the infrastructure. Developers do not have access to the test and production system environments. All changes are checked before they are synchronised with these environments.

#### Source code quality and auditability

This chapter gives an overview of the software tools used and of auditability for analysing and ensuring source code quality.

##### Software tools

The quality of the source code is ensured through the use of various software tools. The tools and some of their features are listed below.

- SonarQube software

  - Sonar is used to check the source code, the coding against a defined set of rules. The set of rules is available in profiles. The profile used is called "Sonar Way". At the end of each sprint, a report is created and evaluated in the sprint review.

  - The source code is publicly available and so is the "Sonar Way" profile that is used. This allows independent inspections with the same set of rules of the source code.

  - Sonar results are evaluated based on classification and severity.

  - Identified issues are resolved.

- Fortify software

  - Fortify is used to check application security

  - The results are evaluated according to the classification and severity.

  - Identified issues are resolved.

- JFrog Xray software

  - JFrog Xray is used to check continuous safety and universal artefact analysis.

  - The results are evaluated according to the classification and severity.

  - Identified issues are resolved.

An overview of the analyses carried out with the above-mentioned software tools is published regularly for the community. The overview provides information on software quality. An example can be found at the following link.
Link: [Example -- overview of software quality](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/README.md#code-quality)

###### Source code auditability

One requirement from the e-voting regulation is: "The source code must be created and documented according to best practices". This concept is known as auditability. To ensure auditability, the source code is audited by external firms. The companies SIG [(](https://www.softwareimprovementgroup.com/)<https://www.softwareimprovementgroup.com/>) and S&P [(https://www.sieberpartners.com/)](https://www.sieberpartners.com/) have a model in place for this purpose. The source code, test artefacts and documents to be published are analysed and checked according to this model. The results of the audit are listed in an auditability report. The auditability report is available [here](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Reports).

#### Testing

Testing of the e-voting system is carried out by the development team and by a dedicated test team. A test concept defines the tests to be carried out. Care is taken to ensure that the test concept can be implemented pragmatically. The defined test cases of the different test types cover the non-functional, the functional and security-specific requirements of the e-voting system. In addition to the manual tests, automated tests are also carried out. The development team performs unit tests, integration tests and end-to-end tests. Further information on the tests is listed in the test concept. The test concept is available [here](../Testing) .

#### Reproducible build and deployment

The reproducible build is understood to be a process for deterministic compilation of the source code. The reproducible build is important for cooperation with the community. The public can use it to check and be sure that the published version is the same as the one created. For this purpose, hash values generated during the compilation are compared. To make a reproducible build possible, it is ensured during development that no non-determinism elements are used (e.g. time stamps in a manifest file).

More information on reproducible builds can be found here: <https://reproducible-builds.org/>

## E-voting system toolchain

This chapter on the toolchain gives an overview of the tools used for the development of the e-voting system. The tools used in software development are state of the art and widely used. They allow a high degree of automation for the build and deployment process. The tools used comply with the specifications and standards of the entire software development process at Swiss Post. The e-voting system is developed according to these specifications and standards. The following graphic illustrates the toolchain.

![Graphic of toolchain for e-voting](../.gitlab/media/software_development_process/image5.png)

The tools used and the main area of application are listed below.

### Tools

- [Atlassian Bit Bucket](https://bitbucket.org/)

  - Code repository

  - Source code versioning

  - Supports CI/CD

- [Jenkins](https://www.jenkins.io/)

  - Automation server

  - Used for the creation of the software components

  - Used for the automated test

- [JFrog Artifactory](https://jfrog.com/)
  - Artefact repository (Docker images, jars, etc.)

- [Docker](https://www.docker.com/)

  - Software artefacts -- container runtime

  - Used for the e-voting system and for Kubernetes

- [SonarQube](https://www.sonarqube.org/)

  - Code quality

  - Code security

- [Xray](https://jfrog.com/xray/)

  - Continuous security

  - Universal artefact analysis

  - Third-party licence checks

- [Fortify](https://www.microfocus.com/de-de/products/static-code-analysis-sast/overview)

  - Static code analyser

- [Kubernetes](https://kubernetes.io/)

  - Automated container deployment, scaling and management

- [GitLab](https://gitlab.com/)

  - DevOps platform

  - Source code management for publication
  
  
## Software specification and implementation of mathematical algorithms

The e-voting system contains complex mathematical algorithms. The algorithms are specified with LaTeX documentation software. With LaTeX, complex mathematical notations can be described very precisely. The use of the software allows for easy testing and implementation, which is a key element for the auditability of the system. Further information on LaTeX can be found here: <https://www.latex-project.org/>.

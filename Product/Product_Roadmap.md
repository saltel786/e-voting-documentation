# Product Roadmap

The roadmap sets out the dates for the availability of releases and documents that are planned to be made available. The planning is currently made up to the production release and will be extended later.

In addition, the information on GitLab is always up to date:

- [Overall Changelog](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/CHANGELOG.md)
- [Readme E-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting/-/blob/master/README.md)
- [Readme Crypto-Primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/README.md)
- [Readme Crypto-Primitives Typescript](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts/-/blob/master/README.md)
- [Readme Crypto-Primitives Domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain/-/blob/master/README.md)
- [Readme Verifier](https://gitlab.com/swisspost-evoting/verifier/verifier/-/blob/master/README.md)



----------

## Upcoming Releases 2022
Address findings from the ongoing independent examination [mandated by the Federal Chancellery](https://www.bk.admin.ch/bk/en/home/dokumentation/medienmitteilungen.msg-id-84337.html), as set out in the plan of action.

The following artefacts will be updated:


#### E-voting System, October 2022
Address the following issues from <https://gitlab.com/swisspost-evoting/e-voting/e-voting#known-issues>:
- Support write-in elections.
- Implement the generation of the eCH-0222 file in the Secure Data Manager.

#### E-voting System, December 2022
- Bug Fix release



----------

## Future Releases (after Relaunch)

- Address further known issues and future work 
- Further findings of lower priority from the ongoing independent examination [mandated by the Federal Chancellery](https://www.bk.admin.ch/bk/en/home/dokumentation/medienmitteilungen.msg-id-84337.html) will be addressed in future improvements, in line with already known [measures communicated from the Federal Chancellery](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/Final%20report%20SC%20VE_November%202020.pdf.download.pdf/Final%20report%20SC%20VE_November%202020.pdf).


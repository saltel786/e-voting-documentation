# Sicherheitsratschläge

Die Stimmberechtigten können selbst den korrekten Verlauf bei der elektronischen Stimmabgabe sicherstellen, indem sie den Prozess gemäss Anleitung durchführen. Dazu gehört insbesondere, dass sie die Prüfcodes, die vor der Stimmabgabe generiert werden, mit den auf den physischen Stimmunterlagen erhaltenen Prüfcodes abgleichen. Um die Stimme zu übermitteln, wird der Bestätigungscode von den physischen Stimmunterlagen auf das verwendete Gerät abgetippt. Auch der Finalisierungscode, der nach der Übermittlung der Stimme angezeigt wird, muss mit dem Finalisierungscode auf den physischen Stimmunterlagen abgeglichen werden. Dieses Verfahren heisst individuelle Verifizierbarkeit. Damit können die Stimmberechtigten überprüfen, ob ihre Stimme unverändert übermittelt und korrekt in der elektronischen Urne registriert worden ist.

Zusätzlich können Stimmberechtigte verschiedene Prüfungen an dem für die Stimmabgabe verwendeten Gerät vornehmen, um sicherzugehen, dass die eigene Stimme nicht manipuliert wird.

Nachstehend sind die verschiedenen Massnahmen beschrieben.

## Überprüfung des Fingerprints des Zertifikats

Wenn Sie überprüfen möchten, ob Sie auf dem korrekten, offiziellen E-Voting-Portal sind, können Sie den Fingerprint des Zertifikats überprüfen. Tun Sie dies vor der elektronischen Stimmabgabe, können Sie ausschliessen, dass Sie sich auf einer manipulierten oder gefälschten Seite befinden.

| [![google-chrome](../icons/google-chrome.png)](#) | [![Internet-Explorer](../icons/microsoft-internet-explorer.png)](#) | [![Microsoft-Edge](../icons/microsoft-edge.png)](#) | [![firefox](../icons/firefox.png)](#) | [![apple-safari](../icons/apple-safari.png)](#) |
| ----------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Google Chrome](zert_chrome.md)           | [Internet Explorer](zert_internetexplorer.md)                | [Edge](zert_edge.md)                                         | [Firefox](zert_firefox.md)                                   | [Safari](zert_safari.md)                                    |

### Was mache ich, wenn mein Browser einen falschen Fingerprint anzeigt?

Sie sollten den Stimmprozess abbrechen und die zuständige Stelle beim Kanton (Supportteam) informieren.

Ein falsch angezeigter Fingerprint des Webseitenzertifikats bedeutet, dass keine direkte Verbindung zwischen dem Webbrowser und der Stimmplattform besteht. Die Unterbrechung wird in den meisten Fällen nicht mit einem Missbrauchsversuch sondern im Gegenteil mit einer Schutzmassnahme in Verbindung stehen. Beispielsweise unterbrechen bestimmte Unternehmen in ihren Netzwerken die Verbindung zwischen den Computern der Mitarbeitenden und den Internetplattformen, um den Datenverkehr auf schädliche Daten hin zu filtern. Auch Virenschutzprogramme können auf diese Weise eingesetzt werden und dementsprechend konfiguriert sein.

## Browserverlauf löschen

Wenn Sie sicher sein wollen, dass auf Ihrem Gerät (Computer, Tablet, Smartphone) keine Rückschlüsse auf Ihre Stimmabgabe getätigt werden können, empfehlen wir Ihnen, nach jeder Stimmabgabe den Browsercache zu leeren. Klicken Sie unten auf den Browser, den Sie verwenden, um zur Anleitung zu gelangen, wie man den Browserverlauf löscht.

| [![google-chrome](../icons/google-chrome.png)](#) | [![Internet-Explorer](../icons/microsoft-internet-explorer.png)](#) | [![Microsoft-Edge](../icons/microsoft-edge.png)](#) | [![firefox](../icons/firefox.png)](#) | [![apple-safari](../icons/apple-safari.png)](#) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Google Chrome](del_chrome.md)                               | [Internet Explorer](del_internetexplorer.md)                 | [Edge](del_edge.md)                                          | [Firefox](del_firefox.md)                                    | [Safari](del_safari.md)                                      |
| [Chrome Mobile](del_chromemobile.md)                         |                                                              | [Edge Mobile](del_edgemobile.md)                             |                                                              | [Safari Mobile](del_safarimobile.md)                         |

## Hashwerte überprüfen (erweiterte Prüfung)

Um sicherzustellen, dass der Javascript-Code auf dem Gerät des/der Stimmberechtigten die vorhergesehenen Operationen durchführt und die Stimme korrekt verschlüsselt, kann der/die Stimmberechtigte die Integrität des Javascript Codes und des verwendeten Root-Zertifikats prüfen. Dafür kann der/die Stimmberechtigte die Hashwerte der zwei betroffenen Javascript-Dateien mit den Werten vergleichen, die auf dem Portal zur elektronischen Stimmabgabe publiziert sind. Diese sind im Seitenquelltext des Portals zur elektronischen Stimmabgabe zu finden.

| [![Microsoft-Edge](../icons/microsoft-edge.png)](#) | [![microsoft-windows](../icons/microsoft-windows.png)](#) | [![apple](../icons/apple.png)](#) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Browser](hash_browser.md)                                   | [Windows](hash_windows.md)                                   | [macOS](hash_macos.md)                                       |

## Browsermodus ohne Add-ons nutzen

Als weitere Vorsichtsmassnahme empfehlen wir Ihnen die Nutzung eines Browsermodus, der Browser-Add-ons standardmässig unterbindet. Bei Chrome oder Microsoft Edge ist dies der Inkognito-Modus, beim Internet Explorer der InPrivate-Modus und im Safari der private Surfmodus. Sie können auch manuell die Add-ons und Browser-Erweiterungen prüfen und gegebenenfalls deaktivieren.

# Firefox

## Überprüfung Zertifikat

![Print Screen Zertifikatsüberprüfung im Firefox Explorer Schritt 1](../img/rules/firefox/de/z1.png)
![Print Screen Zertifikatsüberprüfung im Firefox Explorer Schritt 2](../img/rules/firefox/de/z2.png)
![Print Screen Zertifikatsüberprüfung im Firefox Explorer Schritt 3](../img/rules/firefox/de/z3.png)
![Print Screen Zertifikatsüberprüfung im Firefox Explorer Schritt 4](../img/rules/firefox/de/z4.png)

1. Klicken Sie auf grüne Schloss neben der Adresszeile und dann auf den Pfeil **>** .
2. Wählen Sie **"Weitere Informationen"**.
3. Klicken Sie unter dem Menü **Sicherheit** auf **"Zertifikat anzeigen"**.
4. Vergleichen Sie diesen Fingerabdruck mit demjenigen auf Ihrem Stimmrechtsausweis. Er **muss identisch sein**.
# Google Chrome

## Überprüfung Zertifikat

![Print Screen Zertifikatsüberprüfung in Chrome Schritt 1 und 2](../img/rules/chrome/de/z1.png)
![Print Screen Zertifikatsüberprüfung in Chrome Schritt 3](../img/rules/chrome/de/z2.png)

1. Klicken Sie auf die Schaltfläche **"Website-Informationen anzeigen"**.
2. Klicken Sie auf **"Gültig"** unterhalb des Zertifikats.
3. Wählen Sie den Reiter **"Details"**
4. Klicken Sie auf das Feld **"Fingerabdruck"** und vergleichen den Wert mit demjenigen auf Ihrem Stimmrechtsausweis. Er muss identisch sein.
# Anleitung zur Überprüfung der Web-Adresse des E-Voting Portals

Prüfung des digitalen Fingerabdrucks (Fingerprint) im Browser

- Wenn Sie überprüfen möchten, ob Sie auf dem korrekten, offiziellen E-Voting-Portal sind, können Sie den digitalen Fingerabdruck im Browser überprüfen.
- Tun Sie dies vor der elektronischen Stimmabgabe, können Sie ausschliessen, dass Sie sich auf einer manipulierten oder gefälschten Webseite befinden.

## Microsoft Edge

![img](../img/rules/edge/de/01.png)
![img](../img/rules/edge/de/11.png)
![img](../img/rules/edge/de/12.png)
![img](../img/rules/edge/de/13.png)
![img](../img/rules/edge/de/14.png)
![img](../img/rules/edge/de/15.png)
![img](../img/rules/edge/de/16.png)

1. Nehmen Sie ihren **Stimmausweis** zur Hand und suchen Sie nach dem **digitalen Fingerabdruck (Fingerprint)**.
2. Öffnen Sie den **Browser**, klicken Sie auf das **Schloss** in der Adresszeile und dann auf den Eintrag **Verbindung ist sicher**:.
3. Klicken Sie auf das **Zertifikats**-Symbol.
4. Klicken Sie auf das Register **Details**.
5. Scrollen Sie zum Eintrag **Fingerabdruck** und klicken Sie darauf.
6. Vergleichen Sie den angezeigten **Fingerabdruck** mit dem Fingerabdruck des Stimmausweises:
   **Sind die beide Zeichenfolgen identisch, befinden sie sich auf dem richtigen E-Voting Portal** (Doppelpunkte können Sie ignorieren).

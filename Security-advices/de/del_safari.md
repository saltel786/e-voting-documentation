# Apple Safari

## Browserverlauf löschen in Safari

![Print Screen Browserverlauf löschen in Safari Schritt 1 und 2](../img/rules/safari/de/1.png)
![Print Screen Browserverlauf löschen in Safari Schritt 3 und 4](../img/rules/safari/de/2.png)

1. Klicken Sie auf den Menüpunkt **"Safari"**.
2. Klicken Sie auf den Menüpunkt **"Verlauf löschen ..."**.
3. Wählen Sie in der Dropdown-Liste mindestens den Zeitraum, der den Vorgang Ihrer elektronischen Stimmabgabe umfasst. Wählen Sie zum Beispiel den Eintrag **"Letzte Stunde"**.
4. Klicken Sie auf die Menütaste **"Verlauf löschen"**.
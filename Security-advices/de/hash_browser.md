# Anleitung zur Prüfung der JavaScript-Dateien des E-Voting Portals im Browser

## Prüfung des Base64-SHA256-Hash-Wertes der JavaScript-Dateien im Browser

- Wenn Sie überprüfen möchten, ob sich die vom offiziellen E-Voting-Portal verwendeten JavaScript-Dateien in unverändertem Zustand befinden, können Sie die jeweiligen Base64-SHA256-Hash-Werte überprüfen.
- Tun Sie dies vor der elektronischen Stimmabgabe, können Sie ausschliessen, dass die JavaScript-Dateien manipuliert oder gefälscht (ersetzt) worden sind.
- Die folgende Beschreibung erfolgt anhand der JavaScript-Datei **platformRootCA.js**, kann aber auch für andere JavaScript-Dateien verwendet werden.

### Microsoft Edge

![img](../img/rules/edge/de/01.png)

1. Suchen Sie auf dem Abstimmungsportal den **Base64-SHA256-Hash** der JavaScript-Dateien **platformRootCA.js**:

![img](../img/rules/edge/de/21.png)

2. Öffnen Sie den **Browser** und dann die **Entwicklungsools** des Browsers auf eine der folgenden Arten:
- Drücken Sie **F12** auf der Tastatur oder
- Drücken Sie **Ctrl+Shift+I** auf der Tastatur oder
- Klicken Sie auf die **Auslassungspunkte**, dann auf **Weitere Tools** gefolgt von **Entwicklungsools**

![img](../img/rules/edge/de/22.png)

3. Klicken Sie auf das Register **Elemente** und öffnen Sie im **Html-Script** unter dem **head-Tag** den **script-Tag** mit der Quelle **platformRootCA.js**:

```javascript
<script src="platformRootCA.js" integrity="..."></script>
```

![img](../img/rules/edge/de/23.png)

4. Vergleichen Sie den angezeigten **Hash-Wert** mit den Hash-Wert des Portals:

**Sind die beide Zeichenfolgen identisch, ist die JavaScript-Datei mit grosser Wahrscheinlichkeit nicht verändert worden.**

![img](../img/rules/edge/de/24.png)

Hintergrund:
- Das **integrity-Attribut** schaltet die **SRI-Prüfung (Subresource Integrity)** des Browsers ein.
- Der Browser lädt die JavaScript-Datei herunter und berechnet dessen **Hash-Wert**.
- Dann vergleicht der Browser diesen Hash-Wert mit dem Hash-Wert des integrity-Attributs.
- Sind die beiden Werte gleich, kann der Browser die JavaScript-Datei ausführen, anderfalls wird die Ausführung blockiert.

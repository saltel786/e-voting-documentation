# Internet Explorer

## Zertifikatsüberprüfung

![Print Screen Zertifikatsüberprüfung im Internet Explorer Schritt 1 und 2](../img/rules/internetexplorer/ie/de/z1.png)
![Print Screen Zertifikatsüberprüfung im Internet Explorer Schritt 3](../img/rules/internetexplorer/ie/de/z2.png)

1. Klicken Sie auf das **Schloss-Symbol** in der Adresszeile.
2. Klicken Sie auf **"Zertifikat anzeigen"**.
3. Scrollen Sie ans Ende und vergleichen Sie den **Fingerabdruck** mit demjenigen auf Ihrem Stimmrechtsausweis.
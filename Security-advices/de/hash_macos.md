# Anleitung zur Prüfung der JavaScript-Dateien des E-Voting-Portals mittels OpenSSL macOS

## Prüfung des Base64-SHA256-Hash-Wertes der JavaScript-Dateien mittels OpenSSL unter macOS

- Wenn Sie überprüfen möchten, ob sich die vom offiziellen E-Voting-Portal verwendeten JavaScript-Dateien in unverändertem Zustand befinden, können Sie die jeweiligen Base64-SHA256-Hash-Werte überprüfen.
- Tun Sie dies vor der elektronischen Stimmabgabe, können Sie ausschliessen, dass die JavaScript-Dateien manipuliert oder gefälscht (ersetzt) worden sind.
- Die folgende Beschreibung erfolgt anhand der JavaScript-Datei **platformRootCA.js**, kann aber auch für andere JavaScript-Dateien verwendet werden.

### Apple Safari unter macOS

![img](../img/rules/macos/safari_logo.png)

1. Suchen Sie auf dem Abstimmungsportal den **Base64-SHA256-Hash** der JavaScript-Dateien **platformRootCA.js**:
![img](../img/rules/edge/de/21.png)
2. Öffnen Sie den **Browser** und überprüfen Sie, ob das Entwicklermenü aktiviert ist:

- Klicken Sie unter Safari auf **Einstellungen**
- Aktivieren Sie unter dem Menüpunkt Erweitert die Checkbox **Menü Entwickler in der Menüleiste anzeigen**
- Schliessen Sie das Fenster
![img](../img/rules/macos/de/1.png)
![img](../img/rules/macos/de/2.png)

3. Während Sie sich auf dem Abstimmungsportal befinden, klicken Sie auf das Menü **Entwickler** und wählen Sie **Webinformationen öffnen** aus.
![img](../img/rules/macos/de/3.png)
4. Klicken Sie auf das Register **Quellen**, dann auf den Eintrag **platformRootCA.js**
Laden sie die JavaScript-Datei **platformRootCA.js** via rechte Maustaste **Datei speichern** herunter, z.B. in den Downloads Ordner
![img](../img/rules/macos/de/5.png)
![img](../img/rules/macos/de/6.png)
![img](../img/rules/macos/de/7.png)
5. Öffnen Sie das Programm **Terminal**
![img](../img/rules/macos/de/8.png)6. Schreiben Sie folgenden Befehl in die Konsole.
Ersetzen Sie dabei den Beispiel- Usernamen oder geben Sie den korrekten Pfad zur heruntergeladenen Datei an:

```sh
openssl dgst -sha256 -binary /Users//Downloads/platformRootCA.js | openssl enc -base64
```

Der **Befehl** enthält zwei sequentiell ausgeführte (pipe) OpenSSL-Befehle:

- **SHA256 binary checksum** (dgst - digest - Auszug)
- **Base64 encoding** (enc - encode - Umwandlung)

7. Führen Sie den Befehl aus, indem Sie die Enter-Taste drücken:
![img](../img/rules/macos/de/9.png)
8. Vergleichen Sie den **Hash-Wert** des Terminal-Resultats mit den Hash-Wert des Portals: **Sind die beide Zeichenfolgen identisch, ist die JavaScript-Datei nicht verändert worden.**![img](../img/rules/macos/de/10.png)

# Firefox

## Browserverlauf löschen in Firefox

![Print Screen Browserverlauf löschen in Firefox Schritt 1 und 2](../img/rules/firefox/de/1.png)
![Print Screen Browserverlauf löschen in Firefox Schritt 3](../img/rules/firefox/de/2.png)
![Print Screen Browserverlauf löschen in Firefox Schritt 4 und 5](../img/rules/firefox/de/3.png)

1. Klicken Sie auf die Menüschaltfläche vom Firefox.
2. Wählen Sie den Menüpunkt **Chronik**.
3. Klicken Sie auf den Menüpunkt **"Neuste Chronik löschen"**. (Tastenkombination: Strg + Umschalt + Entf)
4. Wählen Sie in der Dropdown Liste mindestens den Zeitraum, der den Vorgang Ihrer elektronischen Stimmabgabe umfasst. Wählen Sie zum Beispiel den Eintrag **"Letzte Stunde"**.
5. Klicken Sie auf die Menütaste **"Jetzt löschen"**.
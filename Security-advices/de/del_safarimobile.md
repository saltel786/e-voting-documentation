# Safari Mobile

## Löschen des Browsercaches unter iOS

![Print Screen Browserverlauf löschen unter iOS Schritt 1](../img/rules/safari/mobile/de/1.png)
![Print Screen Browserverlauf löschen unter iOS Schritt 2](../img/rules/safari/mobile/de/2.png)
![Print Screen Browserverlauf löschen unter iOS Schritt 3](../img/rules/safari/mobile/de/3.png)

1. Gehen Sie auf Ihrem iPhone in die **Einstellungen** und wählen Sie **Safari**
2. Wählen Sie **"Verlauf" und "Websitedaten löschen"** aus.
3. Bestätigen Sie die Meldung.

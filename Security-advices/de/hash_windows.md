# Anleitung zur Prüfung der JavaScript-Dateien des E-Voting-Portals mittels OpenSSL Windows

## Prüfung des Base64-SHA256-Hash-Wertes der JavaScript-Dateien mittels OpenSSL

- Wenn Sie überprüfen möchten, ob sich die vom offiziellen E-Voting-Portal verwendeten JavaScript-Dateien in unverändertem Zustand befinden, können Sie die jeweiligen Base64-SHA256-Hash-Werte überprüfen.
- Tun Sie dies vor der elektronischen Stimmabgabe, können Sie ausschliessen, dass die JavaScript-Dateien manipuliert oder gefälscht (ersetzt) worden sind.
- Die folgende Beschreibung erfolgt anhand der JavaScript-Datei **platformRootCA.js**, kann aber auch für andere JavaScript-Dateien verwendet werden.

### Microsoft Edge

![img](../img/rules/edge/de/01.png)

1. Suchen Sie auf dem Abstimmungsportal den **Base64-SHA256-Hash** der JavaScript-Dateien **platformRootCA.js**:
![img](../img/rules/edge/de/21.png)
2. Stellen Sie die Software-Bibliothek **OpenSSL** bereit:
![img](../img/rules/edge/de/32.png)

- als **Source-Code** von GitLab oder
- als Betriebssystem-spezifisches **Kompilat** (z.B. openssl-3.0.0 für Windows)

3. Öffnen Sie den Browser und dann die Entwicklungtsools des Browsers auf eine der folgenden Arten: - Drücken Sie **F12** auf der Tastatur oder

- Drücken Sie **Ctrl+Shift+I** auf der Tastatur oder
- Klicken Sie auf die **Auslassungspunkte**, dann auf **Weitere Tools** gefolgt von **Entwicklungsools**

![img](../img/rules/edge/de/33.png)
4. Klicken Sie auf das Register **Quellen**, dann auf den Eintrag **platformRootCA.js**.
Laden sie die JavaScript-Datei **platformRootCA.js** via rechte Maustaste **Speichern unter** herunter, z.B. auf den Desktop:
![img](../img/rules/edge/de/34.png)
5. Öffnen Sie im Datei-Explorer das Verzeichnis **openssl-3.0.0\openssl-3\x64\bin**.

- In diesem bin-Verzeichnis befindet sich das OpenSSL-Executable **openssl.exe**.
- Verschieben Sie die heruntergeladene JavaScript-Datei **platformRootCA.js** in dieses bin-Verzeichnis:
![img](../img/rules/edge/de/35.png)

6. Öffnen Sie **PowerShell** in dieser Datei-Explorer-Ansicht unter **Datei** und **Windows PowerShell öffnen** (S- befindet sich PowerShell gleich im richtigen Pfad, d.h. im obengenannten bin-Verzeichnis)
![img](../img/rules/edge/de/36.png)
7. Schreiben Sie folgenden PowerShell-Code im PowerShell-Editor (Kopieren-Einfügen):

```powershell
Invoke-Expression ".\openssl.exe dgst -sha256 -binary platformRootCA.js | .\openssl.exe enc -base64"
```

Der **PowerShell-Code** enthält zwei sequentiell ausgeführte (pipe) OpenSSL-Befehle:

- **SHA256 binary checksum** (dgst - digest - Auszug)
- **Base64 encoding** (enc - encode - Umwandlung)
![img](../img/rules/edge/de/37.png)

8. Führen Sie den PowerShell-Code aus, indem Sie die Enter-Taste drücken:
![img](../img/rules/edge/de/38.png)
9. Vergleichen Sie den **Hash-Wert** des PowerShell-Resultats mit den Hash-Wert des Portals:
**Sind die beide Zeichenfolgen identisch, ist die JavaScript-Datei nicht verändert worden.**
![img](../img/rules/edge/de/39.png)

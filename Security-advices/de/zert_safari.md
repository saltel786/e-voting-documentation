# Apple Safari

## Überprüfung Zertifikat

![Print Screen Zertifikatsüberprüfung im Safari Explorer Schritt 1](../img/rules/safari/de/z1.png)
![Print Screen Zertifikatsüberprüfung im Safari Explorer Schritt 2](../img/rules/safari/de/z2.png)
![Print Screen Zertifikatsüberprüfung im Safari Explorer Schritt 3](../img/rules/safari/de/z3.png)

1. Klicken Sie auf das Schloss-Symbol im Browser **in der Adresszeile**.
2. Wählen Sie **"Zertifikat einblenden"**.
3. Scrollen Sie bis ans Ende des Fensters bis Sie **Fingerabdrücke** sehen und vergleichen Sie diesen Code mit demjenigen auf Ihrem Stimmrechtsausweis.
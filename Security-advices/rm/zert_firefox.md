# Firefox

## Verifitgar il certificat

![Printscreen co verifitgar il certificat en Mozilla Firefox pass 1](../img/rules/firefox/de/z1.png)
![Printscreen co verifitgar il certificat en Mozilla Firefox pass 2](../img/rules/firefox/de/z2.png)
![Printscreen co verifitgar il certificat en Mozilla Firefox pass 3](../img/rules/firefox/de/z3.png)
![Printscreen co verifitgar il certificat en Mozilla Firefox pass 4](../img/rules/firefox/de/z4.png)

1. Cliccai sin il marschlos verd dasper la lingia d'adressa e lura sin il paliet **>** .
2. Elegiai **"Weitere Informationen"**.
3. Cliccai sut **segirezza** sin **"Zertifikat anzeigen"**.
4. Cumparegliai l'impronta dal det cun quella sin Voss attest da votar. Ellas **ston esser identicas**.

# Instrucziun per controllar las datotecas da Javascript dal portal per la votaziun electronica cun agid dad OpenSSL per Windows

## Controllar la valur hash Base64-SHA256 da la datoteca da Javascript cun agid dad OpenSSL per macOS

- Sche Vus vulais controllar che las datotecas da Javascript utilisadas dal portal per la votaziun electronica uffizial na sajan betg vegnidas modifitgadas, pudais Vus verifitgar las valurs hash Base64-SHA256 respectivas.
- Sche Vus faschais questa controlla avant che vuschar electronicamain, pudais Vus excluder che las datotecas da Javascript èn vegnidas manipuladas u falsifitgadas (remplazzadas).
- La descripziun sutvart vala per la datoteca da Javascript **platformRootCA.js**.Ins po però era duvrar ella per autras datotecas da Javascript.

### Apple Safari da macOS

![img](../img/rules/macos/safari_logo.png)

1. Tschertgai sin il portal da votaziun il **hash Base64-SHA256** da las datotecas da Javascript **platformRootCA.js**:
![img](../img/rules/edge/rm/21.png)

2. Avri il **navigatur** e controllai, sch'il menu da svilup è activà:

- Giai sin Safari e cliccai sin **Einstellungen**
- Activai sut l'opziun da menu Erweitert il punct **Menü Entwickler in der Menüleiste anzeigen**
- Serrai la fanestra
![img](../img/rules/macos/de/1.png)!
![img](../img/rules/macos/de/2.png)

3. Cura che Vus essas en il portal per la votaziun electronica, cliccai sin il menu **Entwickler** e selecziunai **Webinformationen öffnen**.
![img](../img/rules/macos/de/3.png)

4. Cliccai sin il register Quellen e sin l'opziun **platformRootCA.js**.
   cun cliccar sin la tasta dretga da la mieur e selecziunar **Datei speichern** . Memorisai la datoteca p.ex. en l'ordinatur Downloads.![img](../img/rules/macos/de/5.png)
![img](../img/rules/macos/de/6.png)
![img](../img/rules/macos/de/7.png)
5. Avri il program **Terminal**
![img](../img/rules/macos/de/8.png)

6. Scrivai il suandant cumond en la consola. Remplazzai il num d'utilisader da model u inditgai la via d'access correcta a la datoteca telechargiada:

```sh
openssl dgst -sha256 -binary /Users//Downloads/platformRootCA.js | openssl enc -base64
```

Il **cumond** cuntegna dus cumonds consecutivs (pipe) dad OpenSSL:

- **SHA256 binary checksum** (dgst - digest - extrair)
- **Base64 encoding** (enc - encode - transfurmar)

7. Exequi il cumond cun cliccar sin Enter:
![img](../img/rules/macos/de/9.png)
8. Cumparegliai la **valur hash** dal resultat da Terminal cun la valur hash dal portal:
   **Sche las duas successiuns da segns èn identicas, n'è la datoteca da Javascript betg vegnida modifitgada.**
![img](../img/rules/macos/de/10.png)

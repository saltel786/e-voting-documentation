# Safari Mobile

## Stizzar il cache dal navigatur sut iOS

![Printscreen co stizzar la cronologia dal navigatur sut iOS pass 1](../img/rules/safari/mobile/de/1.png)
![Printscreen co stizzar la cronologia dal navigatur sut iOS pass 2](../img/rules/safari/mobile/de/2.png)
![Printscreen co stizzar la cronologia dal navigatur sut iOS pass 3](../img/rules/safari/mobile/de/3.png)

1. Avri sin Voss iPhone las **configuraziuns** e cliccai sin **Safari**
2. Cliccai sin **"Verlauf" e "Websitedaten löschen"**.
3. Confermai l'annunzia.

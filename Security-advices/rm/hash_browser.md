# Instrucziun per controllar las datotecas da Javascript dal portal per la votaziun electronica en il navigatur

## Controllar la valur hash Base64-SHA256 da las datotecas da Javascript en il navigatur

- Sche Vus vulais controllar che las datotecas da Javascript utilisadas dal portal per la votaziun electronica uffizial na sajan betg vegnidas modifitgadas, pudais Vus verifitgar las valurs hash Base64-SHA256 respectivas.
- Sche Vus faschais questa controlla avant che vuschar electronicamain, pudais Vus excluder che las datotecas da Javascript èn vegnidas manipuladas u falsifitgadas (remplazzadas).
- La descripziun sutvart vala per la datoteca da Javascript **platformRootCA.js**. Ins po però era duvrar ella per autras datotecas da Javascript.

### Microsoft Edge

![img](../img/rules/edge/de/01.png)

1. Tschertgai sin il portal da votaziun il hash **Base64-SHA256** da las datotecas da Javascript **platformRootCA.js**:

![img](../img/rules/edge/de/21.png)

2. Avri il **navigatur** e suenter ils **tools da svilup** dal navigatur. Quai pudais Vus far en ina da las suandantas modas:

    - smatgar **F12** sin la tastatura ubain
    - smatgar **Ctrl+Shift+I** sin la tastatura ubain
    - cliccar sin ils **trais puncts**, suenter sin Weitere Tools e la finala sin **Entwicklungstools**

![img](../img/rules/edge/de/22.png)

3. Cliccai sin il register **Elemente** ed avri en il **script html** sut il **tag head** il **tag script** cun la funtauna: **platformRootCA.js**:

```javascript
<script src="platformRootCA.js" integrity="..."></script>
```

![img](../img/rules/edge/de/23.png)

4. Cumparegliai la valur hash visualisada cun la valur hash dal portal:

**Sche las duas successiuns da segns èn identicas, n'è la datoteca da Javascript cun gronda probabilitad betg vegnida modifitgada.**

![img](../img/rules/edge/de/24.png)

Davos las culissas:

- **L'attribut d'integritad** activescha la **verificaziun SRI (Subresource Integrity)** dal navigatur.
- Il navigatur chargia giu la datoteca da Javascript e calculescha sia **valur hash**.
- Sco proxim cumpareglia il navigatur questa valur hash cun la valur hash da l'attribut d'integritad.
- Sche las duas valurs èn identicas, sa il navigatur exequir la datoteca da Javascript, cas cuntrari vegn ella bloccada.

# Internet Explorer

## Stizzar la cronologia dal navigatur en l'Internet Explorer/p>

![Printscreen co stizzar la cronologia dal navigatur en l'Internet Explorer pass 1, 2 e 3](../img/rules/internetexplorer/ie/de/1.png)
![Printscreen co stizzar la cronologia dal navigatur en l'Internet Explorer pass 4](../img/rules/internetexplorer/ie/de/2.png)

1. Avri il menu da l'Internet Explorer. (cumbinaziun da tastas: ALT + X)
2. Cliccai sin **segirezza**.
3. Cliccai sin **"Browserverlauf löschen...."**. (cumbinaziun da tastas: Strg + Umschalt + Entf)
4. Selecziunai las chaschettas da controlla "Temporäre Internet- und Websitedateien" sco era "Cookies und Websitedaten" e cliccai sin **"Löschen"**.

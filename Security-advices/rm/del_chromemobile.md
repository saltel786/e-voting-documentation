# Google Chrome Mobile

## Stizzar la cronologia dal navigatur en Google Chrome Mobile iOS

![Printscreen co stizzar la cronologia dal navigatur en Google Chrome Mobile pass 1](../img/rules/chrome/de/4.png)
![Printscreen co stizzar la cronologia dal navigatur en Google Chrome Mobile pass 2 e 3](../img/rules/chrome/de/6.png)
![Printscreen co stizzar la cronologia dal navigatur en Google Chrome Mobile pass 4](../img/rules/chrome/de/7.png)

1. Cliccai en Google Chrome sin il buttun da las configuraziuns.
2. Cliccai sin **protecziun da datas**.
3. Elegiai **stizzar las datas dal navigatur**.
4. Cliccai sin **stizzar las datas dal navigatur**.

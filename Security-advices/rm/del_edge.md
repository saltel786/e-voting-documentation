# Microsoft Edge

## Stizzar la cronologia dal navigatur en Microsoft Edge

![Printscreen co stizzar la cronologia dal navigatur en Microsoft Edge pass 1 e 2](../img/rules/internetexplorer/e/de/1.png)
![Printscreen co stizzar la cronologia dal navigatur en Microsoft Edge pass 3](../img/rules/internetexplorer/e/de/2.png)
![Printscreen co stizzar la cronologia dal navigatur en Microsoft Edge pass 4](../img/rules/internetexplorer/e/de/3.png)

1. Cliccai sin il menu da l'Edge.
2. Elegiai las **configuraziuns.**
3. Cliccai sin **elements ch'èn da stizzar**.
4. Selecziunai las chaschettas da controlla "Cookies und gespeicherte Websitedaten" sco era "Zwischengespeicherte Daten und Dateien" e cliccai sin **stizzar**.

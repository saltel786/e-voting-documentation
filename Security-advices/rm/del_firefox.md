# Firefox

## Stizzar la cronologia dal navigatur en Mozilla Firefox

![Printscreen co stizzar la cronologia dal navigatur en Mozilla Firefox pass 1 e 2](../img/rules/firefox/de/1.png)
![Printscreen co stizzar la cronologia dal navigatur en Mozilla Firefox pass 3](../img/rules/firefox/de/2.png)
![Printscreen co stizzar la cronologia dal navigatur en Mozilla Firefox pass 4 e 5](../img/rules/firefox/de/3.png)

1. Cliccai sin il menu dal Firefox.
2. Elegiai la **cronica**.
3. Cliccai sin **"Neuste Chronik löschen"**. (cumbinaziun da tastas: Strg + Umschalt + Entf)
4. Elegiai en la glista da defilar almain il spazi da temp che cumpiglia il mument, cura che Vus avais votà electronicamain. Tscherni per exempel l'endataziun **"Letzte Stunde"**.
5. Cliccai sin **"Jetzt löschen"**.

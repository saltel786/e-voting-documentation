# Contrôle de la valeur hash Base64 SHA256 des fichiers JavaScript au moyen d’OpenSSL sous macOS

## Verifying the Base64-SHA256 hash value of the JavaScript files using OpenSSL for macOS operating systems

- Pour savoir si les fichiers JavaScript utilisés par le portail de vote électronique officiel n’ont pas été altérés, vous pouvez contrôler les valeurs hash Base64 SHA256 correspondantes.
- Si vous procédez à cette vérification avant le vote électronique, vous pouvez exclure toute manipulation ou une falsification (remplacement) des fichiers JavaScript.
- La description suivante se base sur le fichier JavaScript **platformRootCA.js**, mais elle peut être aussi utilisée pour d’autres fichiers JavaScript.

### Apple Safari sous macOS

![img](../img/rules/macos/safari_logo.png)

1. Cherchez sur le portail de vote la valeur hash Base64 SHA256 des fichiers JavaScript **platformRootCA.js**:
![img](../img/rules/edge/fr/21.png)
2. Ouvrez le **navigateur** et vérifiez si le menu Développement est activé:
   - Cliquez sur **Préférences** sous Safari
   - Dans le menu Avancées, cochez la case **«Afficher le menu Développement dans la barre des menus»**
   - Fermez la fenêtre
![img](../img/rules/macos/fr/1.png)
![img](../img/rules/macos/fr/2.png)
3. Pendant que vous êtes sur le portail de vote, cliquez sur le menu **Développement** et sélectionnez **Connecter l’inspecteur web**.
![img](../img/rules/macos/fr/3.png)
4. Cliquez sur l’onglet **Sources**, puis sur l’entrée **platformRootCA.js**.
   Téléchargez le fichier JavaScript **platformRootCA.js**, p. ex. dans le dossier Téléchargements, en faisant un clic droit et en sélectionnant **Enregistrer le fichier**.
![img](../img/rules/macos/fr/5.png)
![img](../img/rules/macos/fr/6.png)
![img](../img/rules/macos/fr/7.png)
5. Ouvrez le programme **Terminal**
![img](../img/rules/macos/fr/8.png)6. Écrivez la commande suivante dans la console.
   Remplacez le nom d’utilisateur cité en exemple ou saisissez le chemin d’accès menant au fichier téléchargé:

   ```sh
   openssl dgst -sha256 -binary /Users//Downloads/platformRootCA.js | openssl enc -base64
   ```

La **commande** contient deux commandes OpenSSL exécutées de manière séquentielle (pipe):

- **Somme de contrôle binaire SHA256** (dgst - digest - condensé)
- **Encodage Base64** (enc - encode - transformation)

7. Exécutez la commande en appuyant sur la touche «Enter»:
![img](../img/rules/macos/fr/9.png)

8. Comparez la **valeur hash** du résultat pour Terminal avec celle du portail:
   **Si les deux chaînes sont identiques, le fichier JavaScript n’a pas été modifié.**
![img](../img/rules/macos/fr/10.png)

# Internet Explorer

## Supprimer l’historique du navigateur dans Internet Explorer

![Capture d’écran de la suppression de l’historique du navigateur dans Internet Explorer, étapes 1, 2 et 3](../img/rules/internetexplorer/ie/fr/1.png)
![Capture d’écran de la suppression de l’historique du navigateur dans Internet Explorer, étape 4](../img/rules/internetexplorer/ie/fr/2.png)

1. Ouvrez le menu d’Internet Explorer (Outils). (Raccourci clavier: ALT + X)
2. Sélectionnez l’option **«Sécurité»**.
3. Sélectionnez l’option **«Supprimer l’historique de navigation...»**(Raccourci clavier: Ctrl + Maj + Suppr)
4. Cochez les cases «Fichiers Internet et fichiers de site Web temporaires» et «Cookies et données de sites Web» puis cliquez sur le bouton **«Supprimer»**.

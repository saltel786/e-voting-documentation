# Instructions relatives au contrôle des fichiers JavaScript du portail de VE dans le navigateur

## Contrôle de la valeur hash Base64 SHA256 des fichiers JavaScript dans le navigateur

- Pour savoir si les fichiers JavaScript utilisés par le portail de vote électronique officiel n’ont pas été altérés, vous pouvez contrôler les valeurs hash Base64 SHA256 correspondantes.
- Si vous procédez à cette vérification avant le vote électronique, vous pouvez exclure toute manipulation ou une falsification (remplacement) des fichiers JavaScript.
- La description suivante se base sur le fichier JavaScript **platformRootCA.js**, mais elle peut être aussi utilisée pour d’autres fichiers JavaScript.

### Microsoft Edge

![img](../img/rules/edge/de/01.png)

1. Cherchez sur le portail de vote la **valeur hash Base64 SHA256** des fichiers JavaScript **platformRootCA.js**:

![img](../img/rules/edge/de/21.png)

2. Ouvrez le **navigateur** et les **outils de développement** du navigateur comme suit:
   - Appuyez sur la touche **F12** ou
   - Appuyez sur les touches **Ctrl+Shift+I** ou
   - Cliquez sur les **points de suspension**, puis sur **Outils supplémentaires et Outils de développement**

![img](../img/rules/edge/de/22.png)

3. Cliquez sur l’onglet **Éléments** et ouvrez dans **html**, sous la balise **head**, la balise script avec la source **platformRootCA.js**:

```javascript
<script src="platformRootCA.js" integrity="..."></script>
```

![img](../img/rules/edge/de/23.png)

4. Comparez la **valeur hash** affichée avec celle du portail:
   **Si les deux chaînes sont identiques, il est fort probable que le fichier JavaScript n’a pas été modifié.**

![img](../img/rules/edge/de/24.png)
Contexte:

- **L’attribut integrity** active le **contrôle SRI (subresource integrity)** du navigateur.
- Le navigateur télécharge le fichier JavaScript et calcule sa **valeur hash.**
- Le navigateur compare ensuite cette valeur avec celle de l’attribut integrity.
- Si les deux valeurs sont identiques, le navigateur peut exécuter le fichier JavaScript. Si ce n’est pas le cas, l’exécution est bloquée.

# Instructions relatives au contrôle des fichiers JavaScript du portail de VE au moyen d’OpenSSL Windows

## Contrôle de la valeur hash Base64 SHA256 des fichiers JavaScript au moyen d’OpenSSL

- Pour savoir si les fichiers JavaScript utilisés par le portail de vote électronique officiel n’ont pas été altérés, vous pouvez contrôler les valeurs hash Base64 SHA256 correspondantes.
- Si vous procédez à cette vérification avant le vote électronique, vous pouvez exclure toute manipulation ou une falsification (remplacement) des fichiers JavaScript.
- La description suivante se base sur le fichier JavaScript **platformRootCA.js**, mais elle peut être aussi utilisée pour d’autres fichiers JavaScript.

### Microsoft Edge

![img](../img/rules/edge/de/01.png)

1. Cherchez sur le portail de vote la **valeur hash Base64 SHA256** des fichiers JavaScript **platformRootCA.js**:
![img](../img/rules/edge/de/21.png)
2. Activez la bibliothèque logicielle **OpenSSL**
![img](../img/rules/edge/de/32.png)
   - en tant que **code source** de GitLab ou
   - en tant que **compilateur** propre au système d’exploitation (p. ex. OpenSSL 3.0 pour Windows)

3. Ouvrez le navigateur et les outils de développement du navigateur comme suit: o Appuyez sur la touche **F12** ou
   - Appuyez sur les touches **Ctrl+Shift+I** ou
   - Cliquez sur les **points de suspension**, puis sur **Outils supplémentaires** et **Outils de développement**

![img](../img/rules/edge/de/33.png)

4. Cliquez sur l’onglet **Sources**, puis sur l’entrée **platformRootCA.js**.
   Téléchargez le fichier JavaScript **platformRootCA.js**, p. ex. sur le bureau, en faisant un clic droit et en sélectionnant **Enregistrer sous:**
![img](../img/rules/edge/de/34.png)
5. Ouvrez dans l’explorateur de fichiers le répertoire **openssl-3.0.0\openssl-3\x64\bin**.
   - Dans le répertoire bin se trouve le fichier exécutable **openssl.exe**.
   - Déplacez le fichier JavaScript téléchargé **platformRootCA.js** dans ce répertoire bin:
![img](../img/rules/edge/de/35.png)
6. Ouvrez **PowerShell** dans l’explorateur de fichiers en cliquant sur **Fichier** et **Ouvrir Windows PowerShell**
   (PowerShell se trouve ainsi tout de suite dans le bon chemin d’accès, c’est-à-dire dans le répertoire bin ci-dessus)
![img](../img/rules/edge/de/36.png)
7. Écrivez le code PowerShell suivant dans l’éditeur PowerShell (copier-coller):

```powershell
Invoke-Expression ".\openssl.exe dgst -sha256 -binary platformRootCA.js | .\openssl.exe enc -base64"
```

   Le **code PowerShell** contient deux commandes OpenSSL exécutées de manière séquentielle (pipe):

- **Somme de contrôle binaire SHA256** (dgst - digest - condensé)
  - **Encodage Base64** (enc - encode - transformation)
![img](../img/rules/edge/de/37.png)

8. Exécutez le code PowerShell en appuyant sur la touche «Enter»:
![img](../img/rules/edge/de/38.png)
9. Comparez la valeur hash du résultat pour PowerShell avec celle du portail:
   **Si les deux chaînes sont identiques, le fichier JavaScript n’a pas été modifié.**
![img](../img/rules/edge/de/39.png)

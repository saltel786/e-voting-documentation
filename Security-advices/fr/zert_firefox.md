# Firefox

## Vérification du certificat

![Capture d’écran de la vérification du certificat dans Firefox Explorer, étape 1](../img/rules/firefox/fr/z1.png)
![Capture d’écran de la vérification du certificat dans Firefox Explorer, étape 2](../img/rules/firefox/fr/z2.png)
![Capture d’écran de la vérification du certificat dans Firefox Explorer, étape 3](../img/rules/firefox/fr/z3.png)
![Capture d’écran de la vérification du certificat dans Firefox Explorer, étape 4](../img/rules/firefox/fr/z4.png)

1. Cliquez sur le cadenas vert près de la ligne de l’adresse puis sur la flèche.
2. Sélectionnez **«Plus d’informations»**.
3. Dans le menu **Sécurité**, cliquez sur **«Afficher le certificat»**.
4. Veuillez comparer cette empreinte digitale avec celle qui figure sur votre carte de vote. Elles **doivent être identiques**.
# Google Chrome Mobile

## Supprimer le cache du navigateur sous iOS

![Capture d’écran de la suppression de l’historique dans Google Chrome Mobile, étape 1](../img/rules/chrome/fr/4.png)
![Capture d’écran de la suppression de l’historique dans Google Chrome Mobile, étape 2](../img/rules/chrome/fr/6.png)
![Capture d’écran de la suppression de l’historique dans Google Chrome Mobile, étape 4](../img/rules/chrome/fr/7.png)

1. Cliquez sur Google Chrome et sélectionner l’option «Paramètres».
2. Cliquez sur **Confidentialité**.
3. Sélectionnez l’option **«Effacer les données de navigation»**
4. Cliquez sur **«Effacer les données de navigation»**.

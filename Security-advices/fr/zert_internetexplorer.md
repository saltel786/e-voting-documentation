# Internet Explorer

## Vérification du certificat

![Capture d’écran de la vérification du certificat dans Internet Explorer, étapes 1 et 2](../img/rules/internetexplorer/ie/fr/z1.png)
![Capture d’écran de la vérification du certificat dans Internet Explorer, étape 3](../img/rules/internetexplorer/ie/fr/z2.png)

1. Cliquez sur le **cadenas** dans la ligne de l’adresse.
2. Cliquez sur **«Afficher les certificats»**
3. Déroulez le menu jusqu’à la fin et comparez l’**empreinte digitale** avec celle qui figure sur votre certificat de capacité civique.
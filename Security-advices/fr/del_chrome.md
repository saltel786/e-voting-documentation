# Google Chrome

## Supprimer l’historique du navigateur dans Google Chrome

![Capture d’écran de la vérification du certificat dans Google Chrome, étapes 1, 2 et 3](../img/rules/chrome/fr/1.png)

1. Cliquez sur le bouton indiqué dans Google Chrome.
2. Sélectionnez l’option**«Plus d’outils»**
3. Sélectionnez l’option **«Effacer les données de navigation»**
   Raccourci clavier: Ctrl + Maj + Suppr

![Capture d’écran de la vérification du certificat dans Google Chrome, étapes 4, 5 et 6](../img/rules/chrome/fr/2.png)

4. Dans la liste déroulante, sélectionnez au moins la période qui englobe votre procédure de vote électronique. Par exemple, sélectionnez l’entrée **«de moins d’une heure»**.
5. Cochez toutes les cases.
6. Cliquez sur **«Effacer les données de navigation»**.
# Safari Mobile

## Supprimer le cache du navigateur sous iOS

![Capture d’écran de la suppression de l’historique du navigateur sous iOS, étape 1](../img/rules/safari/mobile/fr/1.png)
![Capture d’écran de la suppression de l’historique du navigateur sous iOS, étape 2](../img/rules/safari/mobile/fr/2.png)
![Capture d’écran de la suppression de l’historique du navigateur sous iOS, étape 3](../img/rules/safari/mobile/fr/3.png)

1. Entrez dans les **Réglages**de votre iPhone et sélectionnez **Safari**
2. Sélectionnez **«Effacer historique, données du site »**.
3. Confirmez en cliquant sur «Effacer».

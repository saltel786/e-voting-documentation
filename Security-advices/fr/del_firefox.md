# Firefox

## Supprimer l’historique du navigateur dans Firefox

![Capture d’écran de la suppression de l’historique du navigateur dans Firefox, étapes 1 et 2](../img/rules/firefox/fr/1.png)
![Capture d’écran de la suppression de l’historique du navigateur dans Firefox, étape 3](../img/rules/firefox/fr/2.png)
![Capture d’écran de la suppression de l’historique du navigateur dans Firefox, étapes 4 et 5](../img/rules/firefox/fr/3.png)



1. Cliquez sur le menu de Firefox.
2. Sélectionnez l’option **Historique**.
3. Cliquez sur **«Effacer l’historique récent»**.
   (Raccourci clavier: Ctrl + Maj + Suppr) 
4. Dans la liste déroulante, sélectionnez au moins la période qui englobe votre procédure de vote électronique. Par exemple, sélectionnez l’entrée **«la dernière heure»**.
5. Cliquez sur le bouton **«Effacer maintenant»**.
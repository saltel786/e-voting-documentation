# Apple Safari

## Vérification du certificat

![Capture d’écran de la vérification du certificat dans Safari Explorer, étape 1](../img/rules/safari/fr/z1.png)
![Capture d’écran de la vérification du certificat dans Safari Explorer, étape 2](../img/rules/safari/fr/z2.png)
![Capture d’écran de la vérification du certificat dans Safari Explorer, étape 3](../img/rules/safari/fr/z3.png)

1. Cliquez sur le cadenas dans le navigateur**dans la ligne de l’adresse
2. Sélectionnez **«Afficher le certificat»**.
3. Déroulez la fenêtre jusqu’à ce que vous voyiez **Empreintes** et comparez ce code avec celui qui figure sur votre carte de vote.
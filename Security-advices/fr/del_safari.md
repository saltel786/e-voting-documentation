# Apple Safari

## Supprimer l’historique du navigateur dans Safari

![Capture d’écran de la suppression de l’historique du navigateur dans Safari, étapes 1 et 2](../img/rules/safari/fr/1.png)
![Capture d’écran de la suppression de l’historique du navigateur dans Safari, étapes 3 et 4](../img/rules/safari/fr/2.png)

1. Cliquez sur **Safari**.
2. Cliquez sur l’option **Effacer l’historique...**.
3. Dans la liste déroulante, sélectionnez au moins la période qui englobe votre procédure de vote électronique. Par exemple, sélectionnez l’entrée **«la dernière heure»**.
4. Cliquez sur le bouton **«Effacer l’historique»**.

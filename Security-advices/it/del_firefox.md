# Firefox

## Cancellare la cronologia di navigazione in Firefox

![Printscreen della cancellazione della cronologia di navigazione in Firefox passo 1 e 2](../img/rules/firefox/it/1.png)
![Printscreen della cancellazione della cronologia di navigazione in Firefox passo 3](../img/rules/firefox/it/2.png)
![Printscreen della cancellazione della cronologia di navigazione in Firefox passo 4 e 5](../img/rules/firefox/it/3.png)

1. Cliccare sul pulsante del menu di Firefox.
2. Selezionare il punto del menu**Cronologia**.
3. Cliccare sul punto del menu **»Cancella la cronologia recente»**. (Combinazione di tasti: Ctrl + Shift + Del)
4. Selezionare nel menu a tendina almeno l’intervallo di tempo che comprende il processo del vostro voto elettronico. Selezionare per esempio la voce **«Ultima ora»**.
5. Cliccare sul pulsante del menu **«Cancella adesso»**.

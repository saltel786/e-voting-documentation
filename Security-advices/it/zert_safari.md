# Apple Safari

## Controllo del certificato

![Printscreen del controllo del certificato in Safari Explorer passo 1](../img/rules/safari/it/z1.png)
![Printscreen del controllo del certificato in Safari Explorer passo 2](../img/rules/safari/it/z2.png)
![Printscreen del controllo del certificato in Safari Explorer passo 3](../img/rules/safari/it/z3.png)

1. Cliccare sul simbolo del lucchetto nel browser **nella riga dell’indirizzo**.
2. Selezionare **«Visualizza certificato»**.
3. Scorrere fino alla fine della finestra finché appaiono le **impronte** e confrontare questo codice con quello presente sul vostro certificato elettorale.

# Google Chrome Mobile

## Cancellare la cache del browser su iOS

![Printscreen della cancellazione della cronologia di navigazione in Google Chrome Mobile passo 1](../img/rules/chrome/it/4.png)![Printscreen della cancellazione della cronologia di navigazione in Google Chrome Mobile passo 3](../img/rules/chrome/it/6.png)![Printscreen della cancellazione della cronologia di navigazione in Google Chrome Mobile passo 4](../img/rules/chrome/it/7.png)

1. Cliccare sul pulsante del menu di Google Chrome e selezionare il punto «Impostazioni».
2. Cliccare sul punto del menu **«Privacy»**.
3. Selezionare il punto del menu**«Cancella dati di navigazione»**.
4. Cliccare sul pulsante del menu **«Cancella dati di navigazione»**.

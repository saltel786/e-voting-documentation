# Safari Mobile

## Cancellare la cache del browser su iOS

![Printscreen della cancellazione della cronologia di navigazione su iOS passo 1](../img/rules/safari/mobile/it/1.png)
![Printscreen della cancellazione della cronologia di navigazione su iOS passo 2](../img/rules/safari/mobile/it/2.png)
![Printscreen della cancellazione della cronologia di navigazione su iOS passo 3](../img/rules/safari/mobile/it/3.png)

1. Nel vostro iPhone andare su **«Impostazioni»** e selezionare **«Safari»**
2. Selezionare **«Cancella dati siti web e cronologia»**.
3. Confermare il messaggio.

# Internet Explorer

## Cancellare la cronologia di navigazione in Internet Explorer

![Printscreen della cancellazione della cronologia di navigazione in Internet Explorer passo 1, 2 e 3](../img/rules/internetexplorer/ie/it/1.png)
![Printscreen della cancellazione della cronologia di navigazione in Internet Explorer passo 4](../img/rules/internetexplorer/ie/it/2.png)

1. Aprire il pulsante del menu di Internet Explorer. (Combinazione di tasti: ALT + X)
2. Selezionare il punto del menu **«Sicurezza»**.
3. Selezionare il punto del menu **«Elimina cronologia esplorazioni...»**. (Combinazione di tasti: Ctrl + Shift + Del)
4. Selezionare le caselle di controllo «File temporanei Internet e file di siti Web» nonché «Cookie e dati di siti Web» e cliccare sul pulsante del menu.**«Elimina»**.

# Google Chrome

## Controllo del certificato

![Printscreen del controllo del certificato in Chrome passo 1 e 2](../img/rules/chrome/it/z1.png)
![Printscreen del controllo del certificato in Chrome passo 3](../img/rules/chrome/it/z2.png)

1. Cliccate sul pulsante **«Visualizzare informazioni sito web»**.
2. Cliccate quindi su **«Valido»** sotto il certificato.
3. Selezionare la scheda **«Dettagli»**
4. Cliccate sul campo **«Impronta digitale»** e assicuratevi che il valore ivi riportato e quello sul vostro certificato elettorale siano identici.

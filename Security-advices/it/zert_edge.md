# Istruzioni per la verifica dell'indirizzo web del portale di voto elettronic

Verifica dell'impronta digitale nel browser

- Se vuoi verificare di essere sul portale di voto elettronico corretto e ufficiale, puoi controllare l'impronta digitale nel browser.
- Se lo fai prima di esprimere il tuo voto elettronicamente, puoi escludere la possibilità di trovarti su un sito web manipolato o falsificato.

## Microsoft Edge

![img](../img/rules/edge/it/01.png)
![img](../img/rules/edge/it/11.png)
![img](../img/rules/edge/it/12.png)
![img](../img/rules/edge/it/13.png)
![img](../img/rules/edge/it/14.png)
![img](../img/rules/edge/it/15.png)
![img](../img/rules/edge/it/16.png)

1. Prendi la tua scheda di voto e cerca l'impronta digitale.
2. Apri il browser, fai clic sul lucchetto nella barra degli indirizzi, quindi fai clic su La connessione è sicura.
3. Fare clic sull'icona del certificato.
4. Fare clic sulla scheda Dettagli.
5. Scorri fino alla voce Identificazione personale e fai clic su di essa.
6. Confronta l'impronta digitale visualizzata con l'impronta sulla scheda di voto. **Se le due stringhe di caratteri sono identiche, sei sul portale di voto elettronico corretto** (puoi ignorare i due punti).

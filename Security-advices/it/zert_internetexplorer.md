# Internet Explorer

## Controllo del certificato

![Printscreen del controllo del certificato in Internet Explorer passo 1 e 2](../img/rules/internetexplorer/ie/it/z1.png)
![Printscreen del controllo del certificato in Internet Explorer passo 3](../img/rules/internetexplorer/ie/it/z2.png)

1. Cliccare sul **simbolo del lucchetto** nella barra dell’indirizzo.
2. Cliccare su **«Visualizza certificato»**.
3. Scorrere fino alla fine e confrontare questa **impronta** con quella presente sul vostro certificato elettorale.

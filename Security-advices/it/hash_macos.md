# Istruzioni per la verifica dei file JavaScript del portale di voto elettronico tramite OpenSSL macOS

## Verificare in OpenSSL di MacOS i valori hash Base64 SHA256 contenuti nei file JavaScript

- Qualora vogliate accertarvi che i file JavaScript utilizzati dal portale ufficiale per il voto elettronico siano invariati, esiste la possibilità di verificare ogni valore hash Base64 SHA256.
- Questo procedimento è da eseguire prima di esprimere il voto elettronico al fine di escludere la possibilità che i dati JavaScript vengano manipolati o falsificati (sostituiti).
- La seguente descrizione fa riferimento al file JavaScript **platformRootCA.js** ma può essere utilizzata anche per altri file JavaScript.

### Apple Safari di MacOS

![img](../img/rules/macos/safari_logo.png)

1. Cercate sul portale di voto l’hash Base64 SHA256 all’interno dei file JavaScript **platformRootCA.js**:
![img](../img/rules/edge/it/21.png)
2. Aprite il **browser** e verificate che il menu Sviluppo sia attivo:
   - Cliccate su Safari su **Preferenze**
   - Attivate nella checkbox la voce di menu «**Mostra menu Sviluppo nella barra dei menu**»
   - Chiudete la finestra
![img](../img/rules/macos/it/1.png)
![img](../img/rules/macos/it/2.png)
3. Mentre vi trovate sul portale di voto cliccate sulla voce del menu **Sviluppo** e selezionate **Mostra inspector web**.
![img](../img/rules/macos/it/3.png)
4. Cliccate sulla scheda **Sorgenti** e in seguito sulla voce **platformRootCA.js**.
   Scaricate il file JavaScript **platformRootCA.js** e tramite il tasto destro del mouse cliccate su **Salva** per salvare il file, ad es. nella cartella dei download
![img](../img/rules/macos/it/5.png)
![img](../img/rules/macos/it/6.png)
![img](../img/rules/macos/it/7.png)
5. Aprite il programma **Terminal**
![img](../img/rules/macos/it/8.png)
6. Scrivere il seguente comando nella console. Sostituite il nome utente provvisorio o inserite il percorso corretto per poter scaricare il file:

```sh
openssl dgst -sha256 -binary /Users//Downloads/platformRootCA.js | openssl enc -base64
```

Il comando contiene due **comandi** OpenSSL eseguiti in modo sequenziale (pipe):

- **SHA256 binary checksum** (dgst - digest - estratto)
- **Base64 encoding** (enc - encode - trasformazione)

7. Eseguite il comando premendo il tasto invio:
![img](../img/rules/macos/it/9.png)
8. Comparate il valore hash del risultato di Terminal con il valore hash del portale:
   **Se le sequenze di caratteri sono identiche, allora il file JavaScript non è stato modificato.**
![img](../img/rules/macos/it/10.png)

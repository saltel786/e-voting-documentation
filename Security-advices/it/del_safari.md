# Apple Safari

## Cancellare la cronologia di navigazione in Safari

![Printscreen della cancellazione della cronologia di navigazione in Safari passo 1 e 2](../img/rules/safari/it/1.png)
![Printscreen della cancellazione della cronologia di navigazione in Safari passo 3 e 4](../img/rules/safari/it/2.png)

1. Cliccare sul punto del menu **«Safari»**.
2. Cliccare sul punto del menu **«Cancella cronologia...»**.
3. Selezionare nel menu a tendina almeno l’intervallo di tempo che comprende il processo del vostro voto elettronico. Selezionare per esempio la voce **«Ultima ora»**.
4. Cliccare sul pulsante del menu **«Cancella cronologia»**.

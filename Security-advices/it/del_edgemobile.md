# Microsoft Edge Mobile

## Cencellare la cronlogia di navigazione in Microsoft Edge

![Printscreen della cancellazione della cronologia di navigazione in Microsoft Edge passo 1 e 2](../img/rules/internetexplorer/em/it/1.png)
![Printscreen della cancellazione della cronologia di navigazione in Microsoft Edge passo 3](../img/rules/internetexplorer/em/it/2.png)
![Printscreen della cancellazione della cronologia di navigazione in Microsoft Edge passo 4](../img/rules/internetexplorer/em/it/3.png)

1. Cliccare sul pulsante del menu di Edge.
2. Selezionare il punto del menu **«Impostazioni».**
3. Cliccare sul punto del menu **«Scegli gli elementi da cancellare»**.
4. Selezionare le caselle di controllo "Cookie e dati di siti web salvati" nonché "Dati e file memorizzati nella cache" e cliccare sul pulsante del menu **«Cancella»**.

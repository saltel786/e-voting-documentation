# Firefox

## Controllo del certificato

![Printscreen del controllo del certificato in Firefox Explorer passo 1](../img/rules/firefox/it/z1.png)
![Printscreen del controllo del certificato in Firefox Explorer passo 2](../img/rules/firefox/it/z2.png)
![Printscreen del controllo del certificato in Firefox Explorer passo 3](../img/rules/firefox/it/z3.png)
![Printscreen del controllo del certificato in Firefox Explorer passo 4](../img/rules/firefox/it/z4.png)

1. Cliccare sul lucchetto verde accanto alla barra dell’indirizzo e poi sulla freccia.
2. Selezionare **«Ulteriori informazioni»**.
3. Cliccare nel menu **Sicurezza** su **«Visualizza certificato»**.
4. Confrontare questa impronta con quella presente sul vostro certificato elettorale. Deve **essere identica**.

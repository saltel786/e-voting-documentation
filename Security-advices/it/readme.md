# Consigli di sicurezza

Gli elettori possono verificare autonomamente il corretto svolgimento del voto elettronico eseguendo il processo esplicato nelle istruzioni. In particolare, i codici di verifica generati prima di aver espresso il voto devono essere comparati con il codice di verifica ricevuto mediante i documenti fisici di voto.Per trasmettere il voto è necessario trascrivere sul dispositivo utilizzato il codice di conferma presente nella documentazione elettorale fisica.Anche il codice di finalizzazione che viene mostrato in seguito alla trasmissione del voto deve essere comparato con il codice di finalizzazione presente nei documenti fisici. Questa procedura prende il nome di verificabilità individuale. In questo modo gli elettori possono verificare se il loro voto è stato trasmesso senza variazioni e se è stato registrato correttamente all’interno dell’urna elettronica.

Oltretutto gli elettori possono eseguire diversi tipi di modifiche sul dispositivo utilizzato al fine di accertarsi che il proprio voto non sia stato manipolato.

Di seguito sono descritte le diverse misure.

## Verificare il fingerprint del certificato

Per assicurarvi che il portale su cui vi trovate è quello corretto e ufficiale per il voto elettronico, potete verificare il fingerprint del certificato. In tal modo potrete escludere di trovarvi su un sito manipolato o contraffatto. I certificati vengono impiegati per garantire l’autenticità del server web interessato e codificare il collegamento con il server stesso. Questa procedura consente di distinguere i siti web autentici da quelli falsi anche nei casi i cui appaiano visivamente molto simili o del tutto identici.

| [[![google-chrome](../icons/google-chrome.png)](#) | [[![Internet-Explorer](../icons/microsoft-internet-explorer.png)](#) | [[![Microsoft-Edge](../icons/microsoft-edge.png)](#) | [[![firefox](../icons/firefox.png)](#) | [[![apple-safari](../icons/apple-safari.png)](#) |
| ----------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Google Chrome](zert_chrome.md)           | [Internet Explorer](zert_internetexplorer.md)                | [Edge](zert_edge.md)                                         | [Firefox](zert_firefox.md)                                   | [Safari](zert_safari.md)                                    |

### Cosa devo fare se nel mio browser viene visualizzato un fingerprint errato?

È opportuno interrompere la procedura di votazione e informare l’istanza preposta presso il Cantone (team di assistenza).

Se un fingerprint del certificato della pagina web viene visualizzato in modo errato significa che non è presente un collegamento diretto tra il web browser e la piattaforma di voto elettronico. Nella maggior dei casi questa interruzione non deve essere intesa come un tentativo di abuso, bensì, al contrario, come una misura di protezione. Alcune aziende, infatti, interrompono all’interno delle proprie reti il collegamento tra i computer dei collaboratori e le piattaforme internet in modo da bloccare dati potenzialmente dannosi. Anche i programmi antivirus possono essere utilizzati in questo modo e configurati di conseguenza.

## Cancellare la cronologia delle esplorazioni

Per essere sicuri che dal vostro dispositivo (computer, tablet, smartphone) non sia possibile risalire al voto da voi espresso, vi consigliamo di vuotare la cache del browser dopo ogni procedura di voto. Cliccate in basso sul browser di cui fate uso per accedere alle istruzioni sulla cancellazione della cronologia.

| [[![google-chrome](../icons/google-chrome.png)](#) | [[![Internet-Explorer](../icons/microsoft-internet-explorer.png)](#) | [[![Microsoft-Edge](../icons/microsoft-edge.png)](#) | [[![firefox](../icons/firefox.png)](#) | [[![apple-safari](../icons/apple-safari.png)](#) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Google Chrome](del_chrome.md)                               | [Internet Explorer](del_internetexplorer.md)                 | [Edge](del_edge.md)                                          | [Firefox](del_firefox.md)                                    | [Safari](del_safari.md)                                      |
| [Chrome Mobile](del_chromemobile.md)                         |                                                              | [Edge Mobile](del_edgemobile.md)                             |                                                              | [Safari Mobile](del_safarimobile.md)                         |

## Verificare i valori hash (verifica avanzata)

Per assicurarsi che il codice JavaScript sul dispositivo dell’elettore / dell’elettrice esegua le operazioni previste e che il voto venga codificato correttamente, l’elettore / l’elettrice può verificare l’integrità del codice JavaScript e del certificato root utilizzato. Per farlo, l’elettore / l’elettrice confronta i valori hash dei due file JavaScript interessati con i valori pubblicati sul portale di voto elettronico. Questi valori si trovano nel testo sorgente del portale per il voto elettronico.

| [[![Microsoft-Edge](../icons/microsoft-edge.png)](#) | [[![microsoft-windows](../icons/microsoft-windows.png)](#) | [[![apple](../icons/apple.png)](#) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Browser](hash_browser.md)                                   | [Windows](hash_windows.md)                                   | [macOS](hash_macos.md)                                       |

## Impiego della modalità browser senza add-on

Quale altra misura di sicurezza consigliamo di utilizzare una modalità browser che blocca gli add-on per default. In Chrome o Microsoft Edge si tratta della modalità Inkognito, in Internet Explorer della modalità InPrivate e in Safari della modalità di navigazione privata. Potete anche verificare e se necessario disattivare gli add-on e le estensioni del browser anche manualmente.

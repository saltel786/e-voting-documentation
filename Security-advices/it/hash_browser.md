# Istruzioni per verificare nel browser i file JavaScript contenuti nel portale di voto elettronico

## Istruzioni per verificare nel browser i valori hash Base64 SHA256 dei file JavaScript

- Qualora vogliate accertarvi che i file JavaScript utilizzati dal portale ufficiale per il voto elettronico siano invariati, esiste la possibilità di verificare ogni valore hash Base64 SHA256.
- Questo procedimento è da eseguire prima di esprimere il voto elettronico al fine di escludere la possibilità che i dati JavaScript vengano manipolati o falsificati (sostituiti).
- La seguente descrizione fa riferimento al file JavaScript **platformRootCA.js** ma può essere utilizzata anche per altri file JavaScript.

### Microsoft Edge

![img](../img/rules/edge/de/01.png)

1. Cercate sul portale di voto l’**hash Base64 SHA256** all’interno del file JavaScript **platformRootCA.js**:

![img](../img/rules/edge/de/21.png)

2. Aprite il **browser** e in seguito gli **Strumenti di sviluppo** del browser in uno dei seguenti modi:
   - Premete **F12** sulla tastiera o
   - Premete **Ctrl+Shift+I** sulla tastiera o
   - Cliccate sui **puntini di sospensione**, poi su **Altri strumenti** e in seguito su **Strumenti di sviluppo**
![img](../img/rules/edge/de/22.png)
3. Cliccate sulla scheda **Elementi** e aprite il **tag script** con la sorgente**platformRootCA.js** nello **script html** al di sotto del **tag head**

```javascript
<script src="platformRootCA.js" integrity="..."></script>
```

![img](../img/rules/edge/de/23.png)

4. Comparate il **valore hash** apparso con il valore hash del portale:
   **Se le sequenze di caratteri sono identiche, allora è molto probabile che il file JavaScript non sia stato modificato.**

![img](../img/rules/edge/de/24.png)
Premessa:

- **L’attributo di integrità** avvia la **verifica SRI (Subresource Integrity)** del browser.
- Il browser scarica il file JavaScript e calcola il suo **valore hash**.
- Dopodiché il browser compara questo valore hash al valore hash dell’attributo di integrità.
- Se i valori coincidono, allora il browser può eseguire il file JavaScript, altrimenti l’esecuzione viene bloccata.

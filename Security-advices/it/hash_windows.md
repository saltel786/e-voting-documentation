# Istruzioni per la verifica dei file JavaScript del portale di voto elettronico tramite OpenSSL Windows

## Verifica dei valori hash Base64 SHA256 dei file JavaScript tramite OpenSSL

- Qualora vogliate accertarvi che i file JavaScript utilizzati dal portale ufficiale per il voto elettronico siano invariati, esiste la possibilità di verificare ogni valore hash Base64 SHA256.
- Questo procedimento è da eseguire prima di esprimere il voto elettronico al fine di escludere la possibilità che i dati JavaScript vengano manipolati o falsificati (sostituiti).
- La seguente descrizione fa riferimento al file JavaScript **platformRootCA.js** ma può essere utilizzata anche per altri file JavaScript.

### Microsoft Edge

![img](../img/rules/edge/de/01.png)

1. Cercate sul portale di voto l’**hash Base64 SHA256** dei file JavaScript **platformRootCA.js**:
![img](../img/rules/edge/de/21.png)

2. Predisporre la raccolta di librerie OpenSSL:
![img](../img/rules/edge/de/32.png)o Come **codice sorgente** di GitLab o
   - Come **raccolta** specifica legata al dispositivo (ad es. Openssl 3.0.0 per Windows)

3. Aprite il browser e in seguito gli strumenti di sviluppo del browser nei seguenti modi:
   - Premete **F12** sulla tastiera o
   - Premete **Ctrl+Shift+I** sulla tastiera o
   - Cliccate sui **puntini di sospensione**, poi su **Altri strumenti** e in seguito su **Strumenti di sviluppo**

![img](../img/rules/edge/de/33.png)

4. Cliccate sulla scheda **Origini** e in seguito sulla voce **platformRootCA.js**.
   Scaricate il file JavaScript **platformRootCA.js** e tramite il tasto destro del mouse cliccate su **Salva** per salvare il file ad es. sul desktop:
![img](../img/rules/edge/de/34.png)
5. Aprite la directory File Explorer **openssl-3.0.0\openssl-3\x64\bin**.
   - All’interno della directory bin si trova l’executable OpenSSL **openssl.exe**.
   - Trascrivete nella directory bin il file JavaScript **platformRootCA.js** scaricato:
![img](../img/rules/edge/de/35.png)
6. Aprite **PowerShell** nella schermata **File** Explorer alla voce File e in seguito **Windows PowerShell**
   (In questo modo PowerShell si trova nel percorso giusto, ovvero la sopraccitata directory bin)
![img](../img/rules/edge/de/36.png)
7. Scrivete il seguente codice PowerShell nell’editor PowerShell (copia-incolla):

```powershell
Invoke-Expression ".\openssl.exe dgst -sha256 -binary platformRootCA.js | .\openssl.exe enc -base64"
```

Il codice PowerShell contiene due comandi OpenSSL eseguiti in modo sequenziale (pipe):

- **SHA256 binary checksum** (dgst - digest - estratto)
- **Base64 encoding** (enc - encode - trasformazione)
![img](../img/rules/edge/de/37.png)

8. Eseguite il codice PowerShell premendo il tasto invio:
![img](../img/rules/edge/de/38.png)
9. Comparate il **valore hash** del risultato PowerShell con il valore hash del portale:
   **Se le sequenze di caratteri sono identiche, allora il file JavaScript non è stato modificato.**
![img](../img/rules/edge/de/39.png)

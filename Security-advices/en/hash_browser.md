# Instructions on how t- verify the JavaScript files of the e-voting portal in the browser

## Verifying the Base64-SHA256 hash value of the JavaScript files in the browser

- If you would like t- check that the JavaScript files used by the official e-voting portal have not been altered, you can verify the relevant Base64-SHA256 hash values.
- If you d- this before you cast your e-vote, you can rule out the possibility of the JavaScript files being manipulated or falsified (replaced).
- The following description is based on the JavaScript file **platformRootCA.js**, though it may als- be used for other JavaScript files.

### Microsoft Edge

![img](../img/rules/edge/de/01.png)

1. In the voting portal, look for the **Base64-SHA256** hash value of the **platformRootCA.js** JavaScript files:

![img](../img/rules/edge/de/21.png)

2. Open up the **browser** and then open the browser’s **Developer tools** in one of the following ways:

- Press **F12** on the keyboard or
- Press **Ctrl+Shift+I** on the keyboard
- Click on the ... icon, and then on **More tools** , followed by **Developer tools**

![img](../img/rules/edge/de/22.png)

3. Click the tab **Elements** and then, under the **head tag** in the **Html script** , open the **script tag** with the source platformRootCA.js:

```javascript
<script src="platformRootCA.js" integrity="..."></script>
```

![img](../img/rules/edge/de/23.png)

4. Compare the **hash value** displayed with the hash value of the portal:

**if both the strings are identical, it is highly likely the JavaScript file has not been altered.**

![img](../img/rules/edge/de/24.png)

Background:

- The **integrity attribute** acticates the **browser’s SRI (Subresource Integrity)** check.
- The browser downloads the JavaScript file and calculates its **hash value**.
- The browser then compares this hash value with the hash value of the integrity attribute.
- If the tw- values are identical, the browser can execute the JavaScript file, otherwise it’s blocked.

# Safari Mobile

## Deleting the browser cache in iOS

![Print screen for deleting browser history in iOS step 1](../img/rules/safari/mobile/en/1.png)
![Print screen for deleting browser history in iOS step 2](../img/rules/safari/mobile/en/2.png)
![Print screen for deleting browser history in iOS step 3](../img/rules/safari/mobile/en/3.png)

1. In your iPhone, go to **Settings** and select **Safari**
2. Select **Clear history and website data**.
3. Confirm your selection.
# Firefox

## Certificate authentication

![Print screen for certificate authentication in Firefox Explorer step 1](../img/rules/firefox/en/z1.png)
![Print screen for certificate authentication in Firefox Explorer step 2](../img/rules/firefox/en/z2.png)
![Print screen for certificate authentication in Firefox Explorer step 3](../img/rules/firefox/en/z3.png)
![Print screen for certificate authentication in Firefox Explorer step 4](../img/rules/firefox/en/z4.png)

1. Click on the green lock next to the address line and then on the arrow **>** .
2. Select **More information**.
3. Under the **Security** menu, click on **Display certificate**.
4. Compare this fingerprint with that on your voting card. It **must be identical**.
# Google Chrome Mobile

## Deleting the browser cache in iOS

![Print screen for deleting browser data in Google Chrome Mobile step 1](../img/rules/chrome/en/4.png)
![Print screen for deleting browser data in Google Chrome Mobile step 2](../img/rules/chrome/en/6.png)
![Print screen for deleting browser data in Google Chrome Mobile step 4](../img/rules/chrome/en/7.png)

1. Click on the Google Chrome menu button and select the Settings menu.
2. Click on the **Data protection** menu item.
3. Select the **Delete browser data** menu item.
4. Click on the **Delete browser data** menu button.
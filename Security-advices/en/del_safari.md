# Apple Safari

## Deleting browsing history in Safari

![Print screen for deleting browser history in Safari steps 1 and 2](../img/rules/safari/en/1.png)
![Print screen for deleting browser history in Safari steps 3 and 4](../img/rules/safari/en/2.png)

1. Click on the **Safari** menu item.
2. Click on the **Delete history...** menu item.
3. Select in the dropdown list at least the period that includes the process of your electronic voting. For instance, select the **Last hour** entry.
4. Click on the **Delete history** menu item.
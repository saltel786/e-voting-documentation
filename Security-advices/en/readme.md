# Security advices

Voters can ensure the e-voting process is successful by voting according to the instructions. In particular, this includes comparing the choice return codes generated before casting the vote with the codes on the physical voting documents. To cast the vote, the ballot casting key from the physical voting documents is typed onto the device being used to vote. The vote cast code, which is displayed after the vote has been cast, must also be compared with the vote cast code on the physical voting documents. This process is known as individual verifiability. This allows voters to check their vote has been cast unaltered, and that it has been registered correctly in the electronic ballot box.

Additionally, voters can carry out various checks on the device used for voting to ensure their vote has not been manipulated.

The various measures are laid out below.

## Checking the certificate’s fingerprint

If you want to check that you are on the correct official E-voting portal, you can check the certificate’s fingerprint. That way you can make sure that you are not on a manipulated or fake website. Certificates are used to guarantee the authenticity of the web server that you are using and to encrypt the communication connection with the server. This enables you to distinguish between genuine and fake websites, even if they may superficially look similar or the same.

| [![google-chrome](../icons/google-chrome.png)](#) | [![Internet-Explorer](../icons/microsoft-internet-explorer.png)](#) | [![Microsoft-Edge](../icons/microsoft-edge.png)](#) | [![firefox](../icons/firefox.png)](#) | [![apple-safari](../icons/apple-safari.png)](#) |
| ----------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Google Chrome](zert_chrome.md)           | [Internet Explorer](zert_internetexplorer.md)                | [Edge](zert_edge.md)                                         | [Firefox](zert_firefox.md)                                   | [Safari](zert_safari.md)                                    |

### What do I do if my browser displays an incorrect fingerprint?

You should stop the voting process and inform the canton (support team).

An incorrectly displayed web page certificate fingerprint means that there is no direct connection between the web browser and the voting platform. In most cases, the interruption is actually the result of a protective measure rather than being linked to an attempt at fraud. For example, certain companies disrupt the connection between employees' computers and the Internet platforms in their networks to filter data traffic for harmful data. Virus protection programs can also be used in this way and configured accordingly.

## Deleting browsing history

If you want to make sure that no conclusions can be drawn about your vote on your device (computer, tablet, smartphone), we recommend clearing the browser cache after every vote. For instructions on how to delete browsing history, click on the browser that you use below.

| [![google-chrome](../icons/google-chrome.png)](#) | [![Internet-Explorer](../icons/microsoft-internet-explorer.png)](#) | [![Microsoft-Edge](../icons/microsoft-edge.png)](#) | [![firefox](../icons/firefox.png)](#) | [![apple-safari](../icons/apple-safari.png)](#) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Google Chrome](del_chrome.md)                               | [Internet Explorer](del_internetexplorer.md)                 | [Edge](del_edge.md)                                          | [Firefox](del_firefox.md)                                    | [Safari](del_safari.md)                                      |
| [Chrome Mobile](del_chromemobile.md)                         |                                                              | [Edge Mobile](del_edgemobile.md)                             |                                                              | [Safari Mobile](del_safarimobile.md)                         |

## Verify hash values (extended check)

To ensure the JavaScript code on the voter’s device carries out the intended operations and encodes the vote correctly, the voter can verify the integrity of the JavaScript code and the root certificate used. To do this, voters can compare the hash values of the two JavaScript files in question with the values published on the e-voting portal. These can be found in the website source code of the e-voting portal.

| [![Microsoft-Edge](../icons/microsoft-edge.png)](#) | [![microsoft-windows](../icons/microsoft-windows.png)](#) | [![apple](../icons/apple.png)](#) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Browser](hash_browser.md)                                   | [Windows](hash_windows.md)                                   | [macOS](hash_macos.md)                                       |

## Use browser mode without add-ons

As an additional precaution, we recommend that you use a browser mode which prevents browser add-ons by default. In Chrome and Microsoft Edge, this is known as incognito mode, in Internet Explorer as InPrivate Browsing and in Safari as Private Browsing. You can also check and deactivate add-ons and browser extensions yourself if necessary.

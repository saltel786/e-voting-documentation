# Google Chrome

## Certificate authentication

![Print screen for certificate authentication in Chrome steps 1 and 2](../img/rules/chrome/en/z1.png)
![Print screen for certificate authentication in Chrome step 3](../img/rules/chrome/en/z2.png)

1. Click on the **“View site information”** icon.
2. Click on **“Valid”** underneath the certificate.
3. Select the **“Details”** tab.
4. Click on the **“Thumbprint”** field and compare the value with the one on your voting card. It must be identical.
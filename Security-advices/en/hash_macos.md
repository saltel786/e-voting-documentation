# Instructions on verifying the JavaScript files of the e-voting portal using OpenSSL for macOS

## Verifying the Base64-SHA256 hash value of the JavaScript files using OpenSSL for macOS operating systems

- If you would like to check that the JavaScript files used by the official e-voting portal have not been altered, you can verify the relevant Base64-SHA256 hash values.
- If you do this before you cast your e-vote, you can rule out the possibility of the JavaScript files being manipulated or falsified (replaced).
- The following description is based on the JavaScript file **platformRootCA.js**, though it may also be used for other JavaScript files.

### Apple Safari for macOS operating systems

![img](../img/rules/macos/safari_logo.png)

1. In the voting portal, look for the **Base64-SHA256** hash value of the **platformRootCA.js** JavaScript files:
![img](../img/rules/edge/de/21.png)
2. Open the **browser** and check that the developer menu has been activated:

- Click on **Settings** in the Safari dropdown menu
  - Go to “Advanced” and select **Show Develop menu in menu bar**
    - Close the window![img](../img/rules/macos/en/1.png)
 ![img](../img/rules/macos/en/2.png)

3. When you are in the voting portal, click on the **Develop** menu and select **Show Web Inspector**.
![img](../img/rules/macos/en/3.png)
4. Click on the **Sources** tab, then on the entry **platformRootCA.js**.
   - Download the JavaScript file **platformRootCA.js** by clicking the right-hand mouse button and selecting **Save file** , e.g. to the Downloads folder
![img](../img/rules/macos/en/5.png)
![img](../img/rules/macos/en/6.png)
![img](../img/rules/macos/en/7.png)
5. Open the programme **Terminal**
![img](../img/rules/macos/en/8.png)
6. Enter the following command in the Console.
   Be sure to replace the sample username, or enter the correct pathway to the downloaded file:

```sh
openssl dgst -sha256 -binary /Users//Downloads/platformRootCA.js | openssl enc -base64
```

The **command** contains two (pipe) OpenSSL commands that are run sequentially:

- **SHA256 binary checksum** (dgst - digest - Auszug)
- **Base64 encoding** (enc - encode - Umwandlung)

7. Execute the command by pressing the Enter key:
![img](../img/rules/macos/en/9.png)
8. Compare the hash value of the Terminal result with the hash value of the portal:
   **if both the strings are identical, it is highly likely the JavaScript file has not been altered.**

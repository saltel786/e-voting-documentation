# Internet Explorer

## Deleting the browser history in Internet Explorer

![Print screen for deleting browser history in Internet Explorer steps 1, 2 and 3](../img/rules/internetexplorer/ie/en/1.png)
![Print screen for deleting browser history in Internet Explorer step 4](../img/rules/internetexplorer/ie/en/2.png)

1. Open the Internet Explorer menu tab. (Shortcut: ALT + X)
2. Select the **Security** menu item.
3. Select the **Delete browser history...** menu item. (Shortcut: Ctrl + Alt + Del)
4. Tick the Temporary internet and website files as well as Cookies and website data checkboxes and click on the **Delete** menu button.
# Microsoft Edge Mobile

## Deleting browsing history in Microsoft Edge

![Print screen for deleting browser history in Microsoft Edge steps 1 and 2](../img/rules/internetexplorer/em/en/1.png)
![Print screen for deleting browser history in Microsoft Edge step 3](../img/rules/internetexplorer/em/en/2.png)
![Print screen for deleting browser history in Microsoft Edge step 4](../img/rules/internetexplorer/em/en/3.png)

1. Click on the Edge menu button.
2. Select the **Settings** menu item.
3. Click on the **Elements to be deleted** menu item.
4. Tick the Cookies and saved website data as well as Cached data and files checkboxes and click on the **Delete** menu item.
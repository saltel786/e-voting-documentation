# Public Intrusion Test (PIT)

Swiss Post conducts periodical public intrusion tests, during which ethical hackers can attempt to attack on the system and attempts to infiltrate the Swiss Post e-voting infrastructure. This type of testing is also known as a penetration test or “pentest” for short in specialist circles.

|Year|Description|Download|
|:--:|:----------|:-------:|
|2022| E-voting-system: Final report on public intrusion test 08.08.-02.09.2022. Final report in english.|<a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/raw/master/Reports/PublicIntrusionTest/PIT_FinalReport_SwissPost_2022_EN.pdf?inline=false"><img src="../../.gitlab/media/icons/button_download-pdf.png" alt="Download document as PDF" width="96" height="26"></a>|
|2022| E-Voting-System: Abschlussbericht öffentlicher Intrusionstest 08.08.-02.09.2022. Final report in german.|<a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/raw/master/Reports/PublicIntrusionTest/PIT_FinalReport_SwissPost_2022_DE.pdf?inline=false"><img src="../../.gitlab/media/icons/button_download-pdf.png" alt="Download document as PDF" width="96" height="26"></a>|

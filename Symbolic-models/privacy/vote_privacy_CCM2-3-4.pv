(* Properties evaluated in this file: vote privacy *)
(* Vote privacy holds if the attacker cannot distinguish whether Alice votes 0 and Bob votes 1 or Alice votes 1 and Bob 0. *)
(* More precisely, we prove that Alice(0) | Bob(1) ~ Alice(1) | Bob(0) *)
(* In this scenario we consider 2 honest voters (Alice and Bob) plus one dishonest voter. *)

(* Trust assumptions: *)
(* - Setup Component is honest *)
(* - The voting client is honest *)
(* - at least one CCM (CCMx here) is honest *)
(* - each honest CCM checks all the proofs of correct mixing from the previous CCMs *)
(* - all CCR are dishonest as soon the voting phase starts (i.e. they are honest during the setup: they simply provide all their material to the adversary) *)
(* - the voting server is dishonest *)

(* 1. Objects and Types *)

(* Types *)
type agent_id. fun t_agent_id(agent_id) : bitstring [data,typeConverter]. (* The type for any voter pseudonym, from 'ID' *)
type password. fun t_password(password) : bitstring [data,typeConverter]. (* The type for user passwords, from 'A_svk'. *)
type num. fun t_num(num) : bitstring [data,typeConverter]. (* The type for natural numbers and codes *)

(* Utilities *)
free c : channel. (* Public Channel for communications with the adversary# public *)

(* 2. Adversary capabilities and functions *)

(* Public Key Encryption Scheme. Note: Enc(k,m,r) i.e. with key first, then message and randomness. *)
type private_ekey. fun t_private_ekey(private_ekey): bitstring [data,typeConverter]. (* The type for private keys + type converter. *)
type public_ekey. fun t_public_ekey(public_ekey) : bitstring [data,typeConverter]. (* The type for public keys + type converter. *)
fun ske(agent_id) : private_ekey [private]. (* The private enc. key associated to an agent_id # private *)
fun pube(private_ekey) : public_ekey. (* The function to get a public key from the private one # public , noninvertible *)
letfun pke(Id:agent_id) = pube(ske(Id)). (* The public enc. key associated to an agent id # public , invertible *)
(* Modeling: In this model, we do not need anymore the split of Enc(K,M,R) into two sub operators Enc_c1(R),Enc_c2(K,M,R) since no function *)
(* in this model refers to these parts separately. Therefore, we use the very classical modeling of Enc(..) as one operator. *)
fun Enc(public_ekey,bitstring,num) : bitstring. (* The asymmetric encryption function with explicit random. *)
reduc forall Sk:private_ekey, M:bitstring, R:num; Dec(Sk,Enc(pube(Sk),M,R)) = M . (* Decryption *)
reduc forall Pk:public_ekey, M:bitstring, R:num; VerifE(Pk,Enc(Pk,M,R)) = true. (* Checks key - even if this computation is not possible with ElGamal encryptions, it strengthens the attacker capabilities in the model *)
reduc forall Id:agent_id; Get_Id(pube(ske(Id))) = Id. (* Extract Id - even if this computation is not possible with ElGamal encryptions, it strengthens the attacker capabilities in the model *)

(* Symmetric key encryption scheme with authenticated data *) (* Without ghash(c,a) *)
const empty_data:bitstring.
type symmetric_ekey. fun t_symmetric_ekey(symmetric_ekey): bitstring [data,typeConverter]. (* The type for symmetric encryption keys. *)
fun Enc_s(symmetric_ekey,bitstring,bitstring) : bitstring. (* Key, Message, Nonce, Authenticated data *)
reduc forall SKey:symmetric_ekey, M:bitstring, Nonce:num, Data:bitstring; Dec_s(SKey,Enc_s(SKey,M,Data),Data) = M. (* Key, Ciphertext, Associated data *)

(* Key derivation functions *)
fun delta(private_ekey,bitstring) : symmetric_ekey [data].(* The Key Derivation Function 'delta' based on PBKDF2(SVK_id,KEYseed), with salt. # public, invertible *)
fun KSkey(password) : symmetric_ekey [data].(* The key PBKDF2(SVK_id,KEYseed) with a KEYseed fixed. # public invertible *)
fun deltaX(num) : symmetric_ekey [data].(* The Key Derivation Function 'delta' with a fixed 'salt' for Setup.SDM(..). # public, invertible *)

(* Representation of the voting options *)
fun v(num) : num [data]. (* The function from voting choices to voting options # public , invertible *)

(* Hash function *)
fun H(bitstring) : num. (* Models the hash functions RecursiveHash(.) and HashAndSquare(.) in the system specification # public , noninvertible *)

(* Non-interactive zero-knowledge proofs of knowledge *)
(* The 'Exponentiation Proof' and 'Plaintext equality proof' are abstracted inside the *)
(* ZKP and VerifP operators. NOTE : no pk_CCR is used here, because it is assumed in this model that sk_CCR is publicly known. *)
(* Modeling: consequently to abstracting pk_CCR and sk_CCR, this ZKP does not (need to) guarantee the fair use of pk_CCR in CreateVote(..).*)
fun VC(agent_id) : bitstring [data]. (* the random Verification Card for id # public *)
fun pCC(private_ekey,num) : num. (* partial Choice Return Codes, i.e. pCC('k_id','v_i') models 'v_i^k_id' # public *)
fun tild(private_ekey,bitstring): num. (* To exponentiate ciphertexts generated by the function Enc # public , noninvertible *)
(* Note that pCC() and tild() both model an exponentiation. Two symbols are defined for readability purposes. *)

fun ZKP(public_ekey, public_ekey, bitstring, bitstring, bitstring, num, private_ekey) : bitstring.
reduc forall pk_EL:public_ekey, Id:agent_id, J1:num, R:num;
 VerifP(pk_EL, pube(ske(Id)), VC(Id), Enc(pk_EL,t_num(v(J1)),R), t_num(pCC(ske(Id),v(J1))),
 ZKP(pk_EL, pube(ske(Id)), VC(Id), Enc(pk_EL,t_num(v(J1)),R), t_num(pCC(ske(Id),v(J1))), R,ske(Id)))
 = true
.

(* Verifiable mixnet *)
(* We model three voters: two honest ones and one dishonest voter. *)
fun PDec(private_ekey, bitstring):bitstring. (* Partial decryption plus re-encryption, with pub. enc. key abstracted. *)
fun PMix(private_ekey, bitstring, bitstring):bitstring. (* Proof of correct Mixing, with pi_mix and pi_dec *)
fun MixVerify(public_ekey, bitstring, bitstring, bitstring) : bool (* Checks the validity of PMix(..). - even if this function does not occur in the processes defined below, it enables the attacker to perform tests that could help her to distinguish when checking equivalence *)
reduc     forall k:private_ekey, E1:bitstring, E2:bitstring, E3:bitstring;
      	  MixVerify(pube(k), (E1,E2,E3), (PDec(k,E1),PDec(k,E2),PDec(k,E3)), PMix(k, (E1,E2,E3), (PDec(k,E1),PDec(k,E2),PDec(k,E3)))) = true
otherwise forall k:private_ekey, E1:bitstring, E2:bitstring, E3:bitstring;
      	  MixVerify(pube(k), (E1,E2,E3), (PDec(k,E1),PDec(k,E3),PDec(k,E2)), PMix(k, (E1,E2,E3), (PDec(k,E1),PDec(k,E3),PDec(k,E2)))) = true
otherwise forall k:private_ekey, E1:bitstring, E2:bitstring, E3:bitstring;
      	  MixVerify(pube(k), (E1,E2,E3), (PDec(k,E2),PDec(k,E1),PDec(k,E3)), PMix(k, (E1,E2,E3), (PDec(k,E2),PDec(k,E1),PDec(k,E3)))) = true
otherwise forall k:private_ekey, E1:bitstring, E2:bitstring, E3:bitstring;
      	  MixVerify(pube(k), (E1,E2,E3), (PDec(k,E2),PDec(k,E3),PDec(k,E1)), PMix(k, (E1,E2,E3), (PDec(k,E2),PDec(k,E3),PDec(k,E1)))) = true
otherwise forall k:private_ekey, E1:bitstring, E2:bitstring, E3:bitstring;
      	  MixVerify(pube(k), (E1,E2,E3), (PDec(k,E3),PDec(k,E1),PDec(k,E2)), PMix(k, (E1,E2,E3), (PDec(k,E3),PDec(k,E1),PDec(k,E2)))) = true
otherwise forall k:private_ekey, E1:bitstring, E2:bitstring, E3:bitstring;
      	  MixVerify(pube(k), (E1,E2,E3), (PDec(k,E3),PDec(k,E2),PDec(k,E1)), PMix(k, (E1,E2,E3), (PDec(k,E3),PDec(k,E2),PDec(k,E1)))) = true
.

(* 3. Methods and Agents processes *)

(* The derivation of the Verification Card private key *)
letfun GetKey(SVK_id:password,pk_EL:public_ekey,VCks_id:bitstring) =
    let KSkey_id = KSkey(SVK_id) in
    let t_private_ekey(Sk_id:private_ekey) = Dec_s(KSkey_id,VCks_id,t_public_ekey(pk_EL)) in
    Sk_id
.

(* The process of creating a ballot *)				(* VCidpk -> Pk_id alias K_id; VCidsk -> Sk_id alias k_id *)
(* Modeling: as mentioned in the system specification, E2 does not need to be encrypted. We model it as a plaintext. *)
(* Modeling: The random R generated in this process is obtained as an extra argument due to ProVerif's language constraints.				  *)
letfun CreateVote(pk_EL:public_ekey, VC_id:bitstring, J1:num, Pk_id:public_ekey, Sk_id:private_ekey) =
    let V = v(J1) in new R:num;
    let E1 = Enc(pk_EL, t_num(V), R) in
    let E2 = t_num(pCC(Sk_id,v(J1))) in
    let P = ZKP(pk_EL,Pk_id,VC_id,E1,E2, R, Sk_id) in
    (E1, E2, tild(Sk_id,E1), Pk_id, P)
.

(* The verification algorithm to test an encrypted vote *)
(* Modeling: consequently to abstract pk_CCR and sk_CCR, the ZKP does not need to guarantee the fair use of pk_CCR in the encrypted vote. *)
letfun ProcessVote(pk_EL:public_ekey, VC_id:bitstring, B:bitstring) =
    let (E1:bitstring, E2:bitstring, EC:num, Pk_id:public_ekey, P:bitstring) = B in
    let Ok = VerifP(pk_EL, Pk_id, VC_id, E1, E2, P) in
    true
.

(* SetupElKey.CCM_i - Generates the CCMs secret keys. *)
free   sk_EL_x : private_ekey [private].		(* for CCMx honest   # private    *)

(* MODELING: Other CCMs' private keys are fixed and public, so they are abstracted away. *)
(* Consequently, no function in this model requires explicitly any of these as argument.  *)

(* MODELING: Since all CCM's key pairs except one are both fixed and public, mergepk(Pk_x) models the   *)
(* merge of the public key Pk_x of the honest CCM with all the public keys of dishonest CCMs.		 *)
fun    mergepk(public_ekey) : public_ekey [data].	(*   # public, invertible since other keys are known *)
letfun pk_EL = mergepk(pube(sk_EL_x)).

(*     'sk_SDM' and 'pk_SDM' are abstracted away *)

(* - Keys, Identifiers and Codes generated for each voter pseudonym *)
(*   Note : the Verification Card key pair (K_id,k_id) <- Gen_e(1^\lambda)  is denoted  (pke(id),ske(id)) *)
fun    SVK(agent_id) : password [private]. (* the Start Voting Key for id	# private *)
reduc  forall Id:agent_id; SVKtoID(SVK(Id)) = Id.	(* the id retrieved from the Start Voting Key *)
fun    BCK(agent_id)    : num       [private]. (* the Ballot Casting Key for id        # private *)
fun    CC(agent_id,num) : bitstring [private]. (* the short Choice Return Codes for id # private *)
fun    VCC(agent_id)    : bitstring [private]. (* the short Vote Cast Return Code, id  # private *)
(* the symbols CCM_table() and VCM_table() are not defined because the setup is assumed to be
honestly generated. The symbols readCC() and readVCC() can immediately return the element
associated to a given key, as defined in the system specification *)

(* public functions to model the CCRs' private keys associated to a given voting card *)
(* kCCR(vcid) is the CCR private key associated to the voting card 'vcid' used to manipulate choice return codes (CC) *)
(* kCCR'(vcid) is the CCR private key associated to the voting card 'vcid' used to manipulate vote cast return code (VCC) *)
fun kCCR(bitstring) : private_ekey [data].
fun kCCR'(nat,bitstring) : private_ekey [data]. (* unlike kCCR which models the keys of the four CCRs as a whole (they are all malicious), kCCR'(i,_) models the key of the i-th CCR. This is necessary to model hlVCC_j and hhlVCC *)

(* - Reductions to retrieve the choice return codes (CC) and the vote cast return code (VCC) *)

(* the CMTable has elements of the form (x,y) with:
x = H(lCC)
y = Enc_s(CC(id), deltaX(lCC)) (with deltaX modelling the KDF function)
and lCC = H(VC(id), (H(pCC)^k_CCR))
*)
reduc forall Id:agent_id, J:num;
readCC( H(t_num(H((VC(Id),tild(kCCR(VC(Id)),t_num(H(t_num(pCC(ske(Id), v(J)))))))))) )
      = Enc_s(
      deltaX(
          H((VC(Id),tild(kCCR(VC(Id)),t_num(H(t_num(pCC(ske(Id), v(J))))))))
      ),
      CC(Id,J),
      empty_data).

(* the CMTable has elements of the form (x,y) with:
x = H(lVCC)
y = Enc_s(VCC(id), deltaX(lVCC)) (with deltaX modelling the KDF function)
and lCC = H(VC(id), (H(BCK^2*k_id)^k_CCR))
*)
fun merge_keys(private_ekey,private_ekey,private_ekey,private_ekey):private_ekey.
reduc forall Id:agent_id, M:bitstring, k1,k2,k3,k4:private_ekey; (* the attacker can multiply exponentiated terms *)
let kCCR_all = merge_keys(kCCR'(1,VC(Id)),kCCR'(2,VC(Id)),kCCR'(3,VC(Id)),kCCR'(4,VC(Id))) in
  merge_exp(tild(kCCR'(1,VC(Id)),M),tild(kCCR'(2,VC(Id)),M),tild(kCCR'(3,VC(Id)),M),tild(kCCR'(4,VC(Id)),M)) = tild(kCCR_all,M).
reduc forall Id:agent_id;
  let kCCR_all = merge_keys(kCCR'(1,VC(Id)),kCCR'(2,VC(Id)),kCCR'(3,VC(Id)),kCCR'(4,VC(Id))) in
readVCC( H(t_num(H((VC(Id),tild(kCCR_all,t_num(H(t_num(tild(ske(Id), t_num(H(t_num(BCK(Id))))))))))))) ) =
  Enc_s(
  deltaX(
    H((VC(Id),tild(kCCR_all,t_num(H(t_num(tild(ske(Id), t_num(H(t_num(BCK(Id)))))))))))
  ),
  VCC(Id),
  empty_data).

(* 4. Phases and Roles *)

free idA, idB : agent_id. (* Defines the identity of two honest voters Alice and Bob  # public *)
free jA1, jB1 : num. (* Voting choices, for the two Honest voters Alice and Bob  # public *)

(* The public data published by the Setup Component or CCRs are made available through public constants and public functions in Setup. *)
(* The only non-available data are the LpCC and the keys of the CMTable built over the pCCs. *)
(* The process Publish_setup(id) reveals to the attacker these data corresponding to the agent 'id' *)
(* Note that these data are published in 'alphabetical' order to prevent one to extract information *)
(* Since such an ordering cannot be modelled in ProVerif we over-approximate it using the
'choice[]' function to swap the two votes on which vote privacy is encoded. *)
(* Indeed, the attacker should not be able to distinguish based on the order of these two outputs. *)
let Publish_setup(pk_EL:public_ekey, id:agent_id, is_target_voter:bool) =
  let Sk_id = ske(id) in
  let VCks_id = Enc_s(KSkey(SVK(id)), t_private_ekey(Sk_id), t_public_ekey(pk_EL)) in
  out(c, VCks_id);

  if is_target_voter then (
    (* Intermediate computations for lpCCA1 and lpCCB1 *)
    let (hpCCA1:num,hpCCB1:num) = (H(t_num(pCC(Sk_id, v(jA1)))), H(t_num(pCC(Sk_id, v(jB1))))) in
    let (lpCCA1:num,lpCCB1:num) = (H((hpCCA1, VC(id))), H((hpCCB1, VC(id)))) in
    (* Intermediate computations for lCCA1 and lCCB1 *)
    let pCA1 = tild(kCCR(VC(id)),t_num(H(t_num(pCC(Sk_id, v(jA1)))))) in
    let pCB1 = tild(kCCR(VC(id)),t_num(H(t_num(pCC(Sk_id, v(jB1)))))) in
    let lCCA1 = H(t_num(H((VC(id),pCA1)))) in
    let lCCB1 = H(t_num(H((VC(id),pCB1)))) in
    (* Intermediate computations for lVCC *)
    let kCCR_all = merge_keys(kCCR'(1,VC(id)),kCCR'(2,VC(id)),kCCR'(3,VC(id)),kCCR'(4,VC(id))) in
    let lVCC_1 = tild(kCCR'(1,VC(id)),t_num(H(t_num(tild(Sk_id, t_num(H(t_num(BCK(id))))))))) in
    let lVCC_2 = tild(kCCR'(2,VC(id)),t_num(H(t_num(tild(Sk_id, t_num(H(t_num(BCK(id))))))))) in
    let lVCC_3 = tild(kCCR'(3,VC(id)),t_num(H(t_num(tild(Sk_id, t_num(H(t_num(BCK(id))))))))) in
    let lVCC_4 = tild(kCCR'(4,VC(id)),t_num(H(t_num(tild(Sk_id, t_num(H(t_num(BCK(id))))))))) in

    let hlVCC_1 = H((lVCC_1,VC(id))) in
    let hlVCC_2 = H((lVCC_2,VC(id))) in
    let hlVCC_3 = H((lVCC_3,VC(id))) in
    let hlVCC_4 = H((lVCC_4,VC(id))) in
    let hhlVCC = H((hlVCC_1,hlVCC_2,hlVCC_3,hlVCC_4,VC(id))) in

    let pVCC = tild(kCCR_all,t_num(H(t_num(tild(Sk_id, t_num(H(t_num(BCK(id))))))))) in
    let lVCC = H(t_num(H((VC(id),pVCC)))) in
    (* ouput of all the data *)
    out(c, choice[lpCCA1,lpCCB1]) | out(c, choice[lpCCB1,lpCCA1]) |
    out(c, choice[lCCA1,lCCB1]) | out(c, choice[lCCB1,lCCA1]) |
    out(c, (hlVCC_1,hlVCC_2,hlVCC_3,hlVCC_4,hhlVCC));
    out(c, lVCC)
  ) |
  !(
  in(c, J:num);
  if not(is_target_voter) || (J <> jA1 && J <> jB1) then
    (* Intermediate computations for lpCC *)
    let hpCC = H(t_num(pCC(Sk_id, v(J)))) in
    let lpCC = H((hpCC, VC(id))) in
    (* Intermediate computations for lCC *)
    let pC = tild(kCCR(VC(id)),t_num(H(t_num(pCC(Sk_id, v(J)))))) in
    let lCC = H(t_num(H((VC(id),pC)))) in
    (* Intermediate computations for lVCC *)
    let kCCR_all = merge_keys(kCCR'(1,VC(id)),kCCR'(2,VC(id)),kCCR'(3,VC(id)),kCCR'(4,VC(id))) in
    let lVCC_1 = tild(kCCR'(1,VC(id)),t_num(H(t_num(tild(Sk_id, t_num(H(t_num(BCK(id))))))))) in
    let lVCC_2 = tild(kCCR'(2,VC(id)),t_num(H(t_num(tild(Sk_id, t_num(H(t_num(BCK(id))))))))) in
    let lVCC_3 = tild(kCCR'(3,VC(id)),t_num(H(t_num(tild(Sk_id, t_num(H(t_num(BCK(id))))))))) in
    let lVCC_4 = tild(kCCR'(4,VC(id)),t_num(H(t_num(tild(Sk_id, t_num(H(t_num(BCK(id))))))))) in

    let hlVCC_1 = H((lVCC_1,VC(id))) in
    let hlVCC_2 = H((lVCC_2,VC(id))) in
    let hlVCC_3 = H((lVCC_3,VC(id))) in
    let hlVCC_4 = H((lVCC_4,VC(id))) in
    let hhlVCC = H((hlVCC_1,hlVCC_2,hlVCC_3,hlVCC_4,VC(id))) in

    let pVCC = tild(kCCR_all,t_num(H(t_num(tild(Sk_id, t_num(H(t_num(BCK(id))))))))) in
    let lVCC = H(t_num(H((VC(id),pVCC)))) in
    (* ouput of all the data *)
    out(c, (lpCC,lCC,lVCC));
    out(c, (hlVCC_1,hlVCC_2,hlVCC_3,hlVCC_4,hhlVCC))
  ).

(* Voting Card for agent 'Id', as produced by the Setup Component during the configuration phase *)
(* Since the Voting Cards are transferred to the Voters in a secure manner (postal mail), this data is made private.          *)
(* The GetAliceData reduction allows the voters to retrieve the codes he needs to vote w.r.t. his voting choices.          *)
fun   AliceData(agent_id) : bitstring [private].
reduc forall Id:agent_id, J1:num; GetAliceData(AliceData(Id),J1) = (SVK(Id), VC(Id), BCK(Id), VCC(Id), CC(Id,J1)).

(* Voting Phase, Voter + Voting Client point of view *)
let Alice_Cmp(InitData:bitstring,J1:num) =
    (* Post-configuration phase --  Gets the Voting Card from the Setup Component through the postal channel *)
    let (SVK_id:password, VC_id:bitstring, BCK_id:num, VCCid:bitstring, CCid1:bitstring) = GetAliceData(InitData,J1) in

    (* Authentication phase --  the specific authentication method used is not modeled.  *)
    out(c, VC_id);						(* Send the Verification Card ID to the Voting System.	*)
    in(c, VCks_id:bitstring);					(* Receive the encrypted Verification Card keystore.	*)
    in(c, pk_EL:public_ekey); (* Receive the public election key *)

    (* Voting phase --  the voting process followed by agent Alice.		*)
    let Sk_id = GetKey(SVK_id,pk_EL,VCks_id) in	(* Recover the Verification Card private key. *)
	  (* Note: 'k_id' -> 'Sk_id' *)
    out(c, CreateVote(pk_EL,VC_id,J1,pube(Sk_id),Sk_id));	(* Creates the voting ballot. *)
    in( c, Rcv_CC_1:bitstring);					(* Receives the Choices Return Codes from the voting server. *)
    if ((Rcv_CC_1=CCid1)) then (				(* Compares the choice return codes. *)
      out(c, pCC(Sk_id,H(t_num(BCK_id))));				(* Confirms the Vote *)
      in( c, =VCCid)						(* Receives and checks the Vote Cast Return Code. *)
    ).


(* The CCMs *)
(* Note that our model assumes here that each CCM checks all the proofs of correct mixing from the previous CCMs	*)
(* Moreover, instead of modelling an honest CCR and CCM as a single component we model an honest CCM which processes well-formed ballots only (macro ProcessVote()). Indeed these ballots must occur in the initial payload that corresponds to the ballots accepted by the honnest CCR. *)
(* We omit the secret sharing scheme of the last sk_EL_4 key  *)
(* We model one single honest CCM. *)

(* In order to model the honest shuffling performed by CCMx, we define a
private channel 'mix' as introduced in: Automated reasoning for equivalences in the applied pi calculus with barriers*, B. Blanchet and B. Smyth (CSF'16) *)
(* This modelling is a sound abstraction of the mix.
It overcomes a well-known limitation of ProVerif without modifying the semantics of the processes. *)
free mix:channel [private].

(* Variants of the PDec, PMix and MixVerify merging together all the proofs produced by dishonest CCMs prior to the honest one. *)
(* The private keys of these CCM are fixed, public and abstracted away; Therefore, the below methods do not take them as arguments.		*)
fun PDec2(bitstring):bitstring [data].			(* Partial decryption plus re-encryption, with pub. enc. key abstracted *)
fun PMix2(bitstring, bitstring):bitstring.		(* Two Proofs of correct Mixing in sequence, with pi_mix and pi_dec.	*)
fun MixVerify2(bitstring, bitstring, bitstring) : bool	(* Checks the validity of PMix2(..).					*)
reduc     forall E1:bitstring, E2:bitstring, E3:bitstring;
     	  MixVerify2((E1,E2,E3), (PDec2(E1),PDec2(E2),PDec2(E3)), PMix2((E1,E2,E3), (PDec2(E1),PDec2(E2),PDec2(E3)))) = true
otherwise forall E1:bitstring, E2:bitstring, E3:bitstring;
     	  MixVerify2((E1,E2,E3), (PDec2(E1),PDec2(E3),PDec2(E2)), PMix2((E1,E2,E3), (PDec2(E1),PDec2(E3),PDec2(E2)))) = true
otherwise forall E1:bitstring, E2:bitstring, E3:bitstring;
      	  MixVerify2((E1,E2,E3), (PDec2(E2),PDec2(E1),PDec2(E3)), PMix2((E1,E2,E3), (PDec2(E2),PDec2(E1),PDec2(E3)))) = true
otherwise forall E1:bitstring, E2:bitstring, E3:bitstring;
      	  MixVerify2((E1,E2,E3), (PDec2(E2),PDec2(E3),PDec2(E1)), PMix2((E1,E2,E3), (PDec2(E2),PDec2(E3),PDec2(E1)))) = true
otherwise forall E1:bitstring, E2:bitstring, E3:bitstring;
      	  MixVerify2((E1,E2,E3), (PDec2(E3),PDec2(E1),PDec2(E2)), PMix2((E1,E2,E3), (PDec2(E3),PDec2(E1),PDec2(E2)))) = true
otherwise forall E1:bitstring, E2:bitstring, E3:bitstring;
      	  MixVerify2((E1,E2,E3), (PDec2(E3),PDec2(E2),PDec2(E1)), PMix2((E1,E2,E3), (PDec2(E3),PDec2(E2),PDec2(E1)))) = true
.

let CCMx(sk_EL_x:private_ekey,pk_EL:public_ekey,idA:agent_id,idB:agent_id) =
   (* Cleansing + check that the encrypted votes of the two honest voters were provided in the first list *)
   in(c, (VC_id1:bitstring,B1:bitstring,VCC1:bitstring));	(* reading from the ballot box *)
   in(c, (VC_id2:bitstring,B2:bitstring,VCC2:bitstring));	(* reading from the ballot box *)
   in(c, (VC_id3:bitstring,B3:bitstring,VCC3:bitstring));	(* reading from the ballot box *)
   if VC_id1=VC(idA) then							(* Ensures that idA voted. *)
   if VC_id2=VC(idB) then							(* Ensures that idB voted. *)
   let Ok1 = ProcessVote(pk_EL,VC_id1,B1) in
   let Ok2 = ProcessVote(pk_EL,VC_id2,B2) in
   let Ok3 = ProcessVote(pk_EL,VC_id3,B3) in
   if VC_id1 <> VC_id2 && VC_id1 <> VC_id3 && VC_id2 <> VC_id3 then		(* Ensures distinct VC_id. *)
   let (E1_1:bitstring, E2_1:bitstring, EC1:num, Pk_id1:public_ekey, P1:bitstring) = B1 in
   let (E1_2:bitstring, E2_2:bitstring, EC2:num, Pk_id2:public_ekey, P2:bitstring) = B2 in
   let (E1_3:bitstring, E2_3:bitstring, EC3:num, Pk_id3:public_ekey, P3:bitstring) = B3 in
   let L_0 = (E1_1,E1_2,E1_3) in
   (* Checks previous CCM's correct mixing, re-encryption and partial decryption together, since they are all dishonest. *)
   in(c, L:bitstring);		(* Requires a list of PDec2(..) of items from L_0, to be checked for MixVerify2(...).	 *)
   (* Modeling/Simplification : Unfortunately, this makes ProVerif loop a lot, and raises the analysis time too high.	 *)
   (* Therefore, since all the private keys of dishonest CCMs are known, we assume that the adversary provides here a     *)
   (* list L such that the ciphers of idA and idB are still in positions one and two. If a dishonest CCM were to mix the *)
   (* list in an other order, the knowledge of it's private keys allows to reorder it with consistent mix/dec proofs.    *)
   let (=PDec2(E1_1),=PDec2(E1_2),=PDec2(E1_3)) = L in
   in(c, pi:bitstring); if MixVerify2(L_0,L,pi) = true then
   (* Mixing, re-encryption and partial decryption *)
   let (E1:bitstring,E2:bitstring,E3:bitstring) = L in
   out(mix, choice[E1,E2]) | out(mix, choice[E2,E1]) |
   in( mix, m1:bitstring ) ; in( mix, m2:bitstring ) ; let m3 = E3 in
   out(c, (PDec(sk_EL_x,m1),PDec(sk_EL_x,m2),PDec(sk_EL_x,m3)));
   out(c, PMix(sk_EL_x,(E1,E2,E3),(PDec(sk_EL_x,m1),PDec(sk_EL_x,m2),PDec(sk_EL_x,m3))))
.

(* Modeling: All partial decryptions after the honest CCM done together, thus allowing to extract the votes.  *)
(*            Note that all the private keys needed to do this are both fixed and public, and thus omitted. *)

reduc forall sk:private_ekey, M:bitstring, R:num;
      Extract(PDec(sk,PDec2(Enc(mergepk(pube(sk)),M,R)))) = M
.

(* 5. Main process, with the choices of honest voters. *)

process
  (* generate LpCC for idA and idB *)
   Publish_setup(pk_EL, idA, true) |
   Publish_setup(pk_EL, idB, true) |

  (* Publishes public data that are not already tagged as public. *)
  out(c, pube(sk_EL_x)); out(c, pk_EL);  (* Publishes the public key of the honest CCM, and the pk_EL composed key. *)
  out(c, pke(idA)); out(c, pke(idB));  (* Publishes the honest voter's public keys. *)

  (* Roles for honest voters. *)
  Alice_Cmp(AliceData(idA),choice[jA1,jB1])
 | Alice_Cmp(AliceData(idB),choice[jB1,jA1])

 (* Dishonest voter(s) : need only one for Observational equivalence. *)
 | (new idC:agent_id; out(c,AliceData(idC)); Publish_setup(pk_EL, idC, false))

 (* The honest CCMx; others are dishonest *)
 | CCMx(sk_EL_x,pk_EL,idA,idB)

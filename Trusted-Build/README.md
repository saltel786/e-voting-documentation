# E-Voting Trusted Build - Scripts

This folder contains all the relevant information needed to perform your own trusted build and compare your hash results with the once from Swiss Post.
Related documentation can be found [here](../Operations/Trusted%20Build%20of%20the%20Swiss%20Post%20Voting%20System.md).

| Folder | Content | Type of file |
|:-------|:--------|:-------------|
|[Release-0.15.2.1](Release-0.15.2.1) | Includes hash values and logs files of the trusted build run of each release. | .md, .html, .txt |
|[Release-0.15.2.1\Protocols](Release-0.15.2.1\Protocols) | Includes the acceptance protocols of the trusted build and the trusted deployment. | .pdf |
|[Scripts](Scripts)| Contains all used script files, of the trusted build run, used in the Kubernetes Jenkins environment of Swiss Post. | scripts |

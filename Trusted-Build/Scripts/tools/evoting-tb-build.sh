#!/bin/bash

#
# (c) Copyright 2022 Swiss Post Ltd.
#

HOSTNAME="$(hostname)"
POSTFIX=""
EVOTING_GIT_URL="https://gitlab.com/swisspost-evoting/e-voting/e-voting.git"
CRYPTO_PRIMITIVES_URL="https://gitlab.com/swisspost-evoting/crypto-primitives"

BRANCH="master"
OUTPUT_DIR="evoting"
TOOLS_DIR="/tools/"
DATA_DIR="/data/"

usage() {
  printf "build.sh [-s <mvnsettingsFile>] [-f] [-b <branchName>] [-e <evoting-git-url>] [-c <crypto-primitives-url>] [-o <outputDirectoryName>] [-t <toolsDir>] [-d <dataDirectory>] [-n <npmrcFile> [-h]\n"
  printf "Options:\n"
  printf "f   Flatten builded executables in outputDirectory. Feature not activated by default\n"
  printf "b   Branch to checkout. % character will be replace with module name (evoting | crypto-primitives | crypto-primitives-domain). Default is ${BRANCH}\n"
  printf "e   evoting project git url. Default is ${EVOTING_GIT_URL}\n"
  printf "c   crypto-primitives project git url. Default is ${CRYPTO_PRIMITIVES_URL}\n"
  printf "o   outputDirectoryName where builded executables will be put. Default is ${OUTPUT_DIR}\n"
  printf "t   directory where the tools are located (iaik files + copy-executables.sh). Default is ${TOOLS_DIR}\n"
  printf "d   directory where the output will be put. Default is ${DATA_DIR}\n"
  printf "n   .npmrc file\n"
  exit 0
}

while getopts ":s:fb:e:c:o:ht:d:n:" arg; do
  case $arg in
    s) MVN_SETTINGS="-s ${OPTARG}"
      ;;
    f) POSTFIX="-${HOSTNAME}"
	     FLATTEN="-f";
      ;;
    b) BRANCH=${OPTARG}
      ;;
    e) EVOTING_GIT_URL=${OPTARG}
      ;;
    c) CRYPTO_PRIMITIVES_URL=${OPTARG}
      ;;
    o) OUTPUT_DIR=${OPTARG}
      ;;
    t) TOOLS_DIR=${OPTARG}
      ;;
    d) DATA_DIR=${OPTARG}
      ;;
    n) NPMRC_FILE=${OPTARG}
      cp ${NPMRC_FILE} ~/.npmrc
      ;;
    h) usage
      ;;
    *) # Display help.
      echo "wrong parameters"
      usage
      ;;
  esac
done

CRYPTO_PRIMITIVES="$(CRYPTO_PRIMITIVES_URL)/crypto-primitives.git"
CRYPTO_PRIMITIVES_DOMAIN="$(CRYPTO_PRIMITIVES_URL)/crypto-primitives-domain.git"
CRYPTO_PRIMITIVES_TS="$(CRYPTO_PRIMITIVES_URL)/crypto-primitives-ts.git"

mkdir ${HOSTNAME}
cd ${HOSTNAME}

git clone ${EVOTING_GIT_URL} evoting
cd evoting
git checkout "$(echo "${BRANCH}" | sed "s=%=evoting=")"

export EVOTING_HOME=$(pwd)

if ! mvn ${MVN_SETTINGS} install:install-file -Dfile=${TOOLS_DIR}/iaikPkcs11Wrapper-1.6.2.jar -DpomFile=${TOOLS_DIR}/iaikPkcs11Wrapper-1.6.2.pom; then
  exit 10
fi

git clone ${CRYPTO_PRIMITIVES} crypto-primitives
cd crypto-primitives
git checkout "$(echo "${BRANCH}" | sed "s=%=crypto-primitives=")"
if ! mvn ${MVN_SETTINGS} clean install -DskipTests; then
  exit 10
fi

cd ..
git clone ${CRYPTO_PRIMITIVES_DOMAIN} crypto-primitives-domain
cd crypto-primitives-domain
git checkout "$(echo "${BRANCH}" | sed "s=%=crypto-primitives-domain=")"
if ! mvn ${MVN_SETTINGS} clean install -DskipTests; then
  exit 10
fi

cd ..
git clone ${CRYPTO_PRIMITIVES_TS} crypto-primitives-ts
cd crypto-primitives-ts
git checkout "$(echo "${BRANCH}" | sed "s=%=crypto-primitives-ts=")"
if ! mvn ${MVN_SETTINGS} -X clean install -DskipTests; then
  exit 10
fi

cd ..
if ! mvn ${MVN_SETTINGS} -X clean install -DskipTests; then
  exit 10
fi

cd ${TOOLS_DIR}
./evoting-tb-copy-artefacts.sh -s ${EVOTING_HOME} -d ${DATA_DIR}/${OUTPUT_DIR}${POSTFIX} ${FLATTEN}
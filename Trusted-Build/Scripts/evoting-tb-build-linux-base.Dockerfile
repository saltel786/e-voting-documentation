#
# (c) Copyright 2022 Swiss Post Ltd.
#

FROM eurolinux/centos-stream-9

USER root

RUN yum install glibc.i686 git bzip2 unzip xz -y

COPY ./certs/appl_proxy_ca /etc/pki/ca-trust/source/anchors/appl_proxy_ca.crt
RUN update-ca-trust
ARG JAVA_URL=https://github.com/adoptium/temurin17-binaries/releases/download/jdk-17.0.3%2B7/OpenJDK17U-jdk_x64_linux_hotspot_17.0.3_7.tar.gz
ARG MAVEN_URL=https://archive.apache.org/dist/maven/maven-3/3.8.5/binaries/apache-maven-3.8.5-bin.tar.gz
ARG NODE_URL=https://nodejs.org/dist/v14.17.5/node-v14.17.5-linux-x64.tar.xz
ARG PHANTOMJS_URL=https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
ARG CHROMIUM_URL=https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
ARG WINE_BINARIES_CENTOS9_URL=https://gitlab.com/swisspost-evoting/e-voting/e-voting/-/package_files/48045366/download

#JAVA
RUN mkdir -p /opt/customtools/java && curl -ksSL -o jdk.tar.gz $JAVA_URL && tar xvzf jdk.tar.gz -C /opt/customtools/java && rm jdk.tar.gz
ENV JAVA_HOME="/opt/customtools/java/jdk-17.0.3+7/"

#UPDATE CERTIFICATES
COPY ./certificates /certificates
RUN for c in `find /certificates -type f -name *.pem`; \
       do $JAVA_HOME/bin/keytool -importcert -alias $(basename -s .pem $c) -storepass changeit -keystore $JAVA_HOME/lib/security/cacerts -noprompt -trustcacerts -file ${c}; \
    done 

#MAVEN
RUN mkdir -p /opt/customtools/maven && curl -sSL -o maven.tar.gz $MAVEN_URL && tar xvzf maven.tar.gz -C /opt/customtools/maven && rm maven.tar.gz
ENV MAVEN_HOME="/opt/customtools/maven/apache-maven-3.8.5/"

#NODE
RUN mkdir -p /opt/customtools/node && curl -sSL -o node.tar.xz $NODE_URL && tar -xf node.tar.xz -C /opt/customtools/node && rm node.tar.xz
ENV NODE_HOME="/opt/customtools/node/node-v14.17.5-linux-x64/"

#PHANTOMJS
RUN mkdir -p /opt/customtools/phantomjs && curl -sSL -o phantomjs.tar.bz2 $PHANTOMJS_URL && tar -xf phantomjs.tar.bz2 -C /opt/customtools/phantomjs \
&& rm phantomjs.tar.bz2 && ln -s /opt/customtools/phantomjs/phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/bin/
ENV PHANTOMJS_HOME="/opt/customtools/phantomjs/phantomjs-2.1.1-linux-x86_64/"

#CHROMIUM
RUN curl -sSL -o google-chrome.rpm $CHROMIUM_URL && yum install google-chrome.rpm -y && rm google-chrome.rpm

#WINE
RUN if [[ -n "$WINE_BINARIES_CENTOS9_URL" ]]; then cd /usr && curl -ksSL -o wine-binaries.tar.gz $WINE_BINARIES_CENTOS9_URL \
&& tar xvzf ./wine-binaries.tar.gz && rm wine-binaries.tar.gz; fi

ENV PATH="${JAVA_HOME}bin/:${MAVEN_HOME}bin/:${NODE_HOME}bin:${PHANTOMJS_HOME}bin/:${PATH}"

#GULP & GRUNT
RUN npm install -g grunt-cli@1.3.2 && npm install -g gulp-cli@2.3.0


RUN useradd -m baseuser

#COPY BUILD ENV AND SET RIGHT RIGHTS
COPY ./settings.xml /home/baseuser
COPY ./evoting-build-images/tools /home/baseuser/tools
COPY ./evoting /home/baseuser/evoting
COPY ./npmrc  /home/baseuser/.npmrc
RUN chown -R baseuser:baseuser /home/baseuser/

USER baseuser
WORKDIR /home/baseuser

RUN if [[ -n "$WINE_BINARIES_CENTOS9_URL" ]]; then wineboot; fi
RUN if [[ -n "$WINE_BINARIES_CENTOS9_URL" ]]; then wineserver -d; fi

CMD ["sh", "-c", "tools/evoting-tb-build.sh -t /home/baseuser/tools/ -s /home/baseuser/settings.xml -d /home/baseuser/export"]
